import { expect, test } from '@playwright/test'
import { PrismaClient } from '@prisma/client'

import { ListPage } from 'e2e/util'
import { getTestUserAfterSeed, reseedTestUser, Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test.afterAll(async () => {
  await reseedTestUser()
})

test('Trash shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Trash)

  const titles = Object.values(Seed.Trash)

  await lp.expectTitles(titles)
})

test('Trashing and restoring is visually immediate', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const title = Seed.Inbox.A
  const row = lp.getRowByTitle(title)

  await row.trash()
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Trash)
  await row.restore()
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Inbox)
  await lp.expectTitles([title])
})

test('Trash page: deleting row is visually immediate', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Trash)

  const title = Seed.Trash.A
  await lp.getRowByTitle(title).delete()
  await lp.expectTitles([title], { NOT: true })
})

test('Trash page: deleting row with tags does not delete thos tags or fail with fk constraint', async ({ page }) => {
  const prisma = new PrismaClient()
  const lp = new ListPage(page)
  await lp.goto(List.Trash)

  const context = Seed.Context.D

  const row = lp.getRowByTitle(Seed.Trash.NA)
  await expect(row.row).toHaveText(new RegExp(context))
  await row.delete()
  const testUser = await getTestUserAfterSeed()
  expect(
    await prisma.tag.findUnique({
      where: { userId_name: { userId: testUser.id, name: context } },
      select: { name: true, userId: true },
    }),
  )
    .toStrictEqual({ name: context, userId: testUser.id })
})

test('Trash page: deleting all is visually immediate', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Trash)

  await page.getByRole('button', { name: 'Delete All Forever' }).click()

  await expect(page.getByTestId('row')).toHaveCount(0)
})
