import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { List } from '~/lib/constants'

test('Toggling dark theme is immediate and persistent', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const light = 'rgb(255, 255, 255)'
  const dark = 'rgb(26, 27, 30)'

  const row = page.getByTestId('row').first()

  await lp.toggleDarkTheme()
  await expect(row).toHaveCSS('background-color', dark)
  await lp.goto(List.Inbox)
  await expect(row).toHaveCSS('background-color', dark)

  await lp.toggleDarkTheme()
  await expect(row).toHaveCSS('background-color', light)
  await lp.goto(List.Inbox)
  await expect(row).toHaveCSS('background-color', light)
})
