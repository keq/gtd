import { expect, test } from '@playwright/test'
import { TagType } from '@prisma/client'

import { ListPage } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

const nextActionTitles = [
  Seed.NextAction.PA.A,
  Seed.NextAction.PB.A,
  Seed.NextAction.A,
  Seed.NextAction.TA,
  Seed.NextAction.TB,
  Seed.NextAction.TC,
  Seed.NextAction.TD,
]

test('Next Actions shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.NextAction)

  await lp.expectTitles(nextActionTitles)
})

test('Next Actions does not show multiple next actions from one project', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.NextAction)

  await lp.expectTitles([Seed.NextAction.PA.C], { NOT: true })
})

test('next actions tags can be added/changed/cleared', async ({ page }) => {
  test.slow()
  const testTags = async (tagType: TagType) => {
    const lp = new ListPage(page)
    await lp.goto(List.NextAction)

    const row = lp.getRowByTitle(Seed.NextAction.A)

    const nameA = Seed[tagType].A
    const nameB = Seed[tagType].B
    await expect(row.row).not.toContainText(nameA)

    // add a tag
    await row.edit({ [tagType === 'Context' ? 'context' : 'waitingFor']: nameA })
    await expect(row.row).toContainText(nameA)
    await lp.goto(List.NextAction)
    await expect(row.row).toContainText(nameA)

    // add different tag
    await row.edit({ [tagType === 'Context' ? 'context' : 'waitingFor']: nameB })
    await expect(row.row).not.toContainText(nameA)
    await expect(row.row).toContainText(nameB)
    await lp.goto(List.NextAction)
    await expect(row.row).toContainText(nameB)

    // clear tags
    await row.edit({ [tagType === 'Context' ? 'context' : 'waitingFor']: null })
    await expect(row.row).not.toContainText(nameB)
    await lp.goto(List.NextAction)
    await expect(row.row).not.toContainText(nameB)
  }
  await testTags('Context')
  await testTags('WaitingFor')
})

test("Next Actions page's next actions have project pointers that open up the project's details when clicked", async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.NextAction)
  await expect(page.getByTestId('project pointer')).toHaveCount(2)

  const project = Seed.Project.A

  await page.getByTestId('project pointer').getByText(project, { exact: true }).click()

  await expect(lp.details.getByRole('textbox')).toHaveValue(project)
})

test('Next Actions page can be filtered by tag', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.NextAction)

  await lp.contextSelect.click()
  await lp.contextSelect.type(Seed.Context.A)
  await lp.contextSelect.press('ArrowDown')
  await lp.contextSelect.press('Enter')
  await page.getByRole('heading').focus()
  await lp.expectTitles([Seed.NextAction.TA])
  await lp.expectTitles([
    Seed.NextAction.TB,
    Seed.NextAction.TC,
    Seed.NextAction.TD,
  ], { NOT: true })

  // make POM of selects
  await lp.contextSelect.click()
  await lp.contextSelect.press('Backspace')
  await page.getByRole('heading').focus()
  await lp.expectTitles(nextActionTitles)

  await lp.waitingForSelect.click()
  await lp.waitingForSelect.type(Seed.WaitingFor.A)
  await lp.waitingForSelect.press('ArrowDown')
  await lp.waitingForSelect.press('Enter')
  await page.getByRole('heading').focus()
  await lp.expectTitles([Seed.NextAction.TC])
  await lp.expectTitles([
    Seed.NextAction.TA,
    Seed.NextAction.TB,
    Seed.NextAction.TD,
  ], { NOT: true })

  // make POM of selects
  await lp.waitingForSelect.click()
  await lp.waitingForSelect.press('Backspace')
  await page.getByRole('heading').focus()
  await lp.expectTitles(nextActionTitles)
})
