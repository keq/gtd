import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test('Completed things that are NOT of a project show up on Archive page', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Archive)
  await lp.expectTitles([Seed.Archive.P_A, Seed.Archive.DI, Seed.Archive.NA])
})

test('Uncompleted next actions of uncollapsed completed projects show up', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Archive)
  await lp.expectTitles([Seed.Archive.P_A_A])
})

test('Completed things that are next actions of an uncompleted/trashed project DO NOT show up on Archive page', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Archive)
  await lp.expectTitles([Seed.Archive.PA_A, Seed.Trash.PA_A], { NOT: true })
})

test('Archive page sorts things by completion datetime', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Archive)
  const rowNames = page.getByTestId('row name')
  await expect(rowNames).toHaveCount(4)
  const titles = await rowNames.allTextContents()

  const objectified = { ...titles }
  const correctOrder = {
    ...[Seed.Archive.NA, Seed.Archive.DI, Seed.Archive.P_A, Seed.Archive.P_A_A],
  }
  expect(objectified).toEqual(correctOrder)
})

test('Unchecking/checking things moves them immediately', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Archive)

  const title = Seed.Archive.NA
  const row = lp.getRowByTitle(title)
  await row.uncheck()
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.NextAction)
  await row.check()
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Archive)
  await lp.expectTitles([title])
})
