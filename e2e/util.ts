import crypto from 'crypto'

import { expect } from '@playwright/test'

import { List, ListPath, ListStr } from '~/lib/constants'

import type { Locator, Page } from '@playwright/test'

export const nonce = () => {
  return crypto.randomBytes(16).toString('hex')
}

class PageOrRow {
  readonly contextSelect: Locator
  readonly waitingForSelect: Locator
  readonly dateInput: Locator
  readonly timeInput: Locator
  readonly menuContextSelect: Locator
  readonly menuWaitingForSelect: Locator
  readonly menuDateInput: Locator
  readonly menuTimeInput: Locator
  constructor(root: Page | Locator) {
    const menu = ('page' in root ? root.page() : root).getByRole('dialog')
    this.contextSelect = root.locator('label').getByText('Context')
    this.waitingForSelect = root.locator('label').getByText('Waiting For')
    this.dateInput = root.locator('label').getByText(/Date.*/)
    this.timeInput = root.locator('label').getByText(/Time.*/)
    this.menuContextSelect = menu.locator('label').getByText('Context')
    this.menuWaitingForSelect = menu.locator('label').getByText('Waiting For')
    this.menuDateInput = menu.locator('label').getByText(/Date.*/)
    this.menuTimeInput = menu.locator('label').getByText(/Time.*/)
  }
}

export class ListPage extends PageOrRow {
  readonly page: Page
  readonly details: Locator

  constructor(page: Page) {
    super(page)
    this.page = page
    this.details = page.getByTestId('details')
  }

  async goto(list: List) {
    await this.page.goto(`/${ListPath[list]}`)
  }

  async expectTitles(titles: readonly string[], opts?: { NOT: boolean }) {
    const { NOT } = opts ?? { NOT: false }
    for (const title of titles) {
      const row = this.getRowByTitle(title).row
      if (NOT) {
        await expect(row).not.toBeVisible()
      } else {
        await expect(row).toBeVisible()
      }
    }

    // sanity check
    await expect(this.page.getByText('𐦀', { exact: true })).not.toBeVisible()
  }

  getRowByTitle(title: string) {
    return new Row(this.page, title)
  }

  async addNonceToInbox() {
    await expect(this.page).toHaveURL(`/${ListPath[List.Inbox]}`)
    const title = nonce()
    await this.page.getByTestId('add button').click()
    const input = this.page.getByTestId('add form').getByRole('textbox')
    await input.fill(title)
    await input.press('Enter')
    return title
  }

  async addNonceToProject(project: string) {
    await expect(this.page).toHaveURL(`/${ListPath[List.Project]}`)
    const title = nonce()

    const proj = this.getRowByTitle(project)
    await proj.row.getByTestId('add button').click()
    const input = proj.row.getByTestId('add form').getByRole('textbox')
    await input.fill(title)
    await input.press('Enter')
    return title
  }

  async toggleDarkTheme() {
    await this.page.getByTitle('User Settings').click()
    await this.page.getByRole('menuitem', { name: 'Theme' }).click()
  }
}

export class Row extends PageOrRow {
  readonly root: Page | Locator
  readonly row: Locator
  readonly name: Locator
  readonly checkbox: Locator
  readonly menuButton: Locator
  readonly collapseButton: Locator

  constructor(root: Page | Locator, title: string) {
    super(root)
    this.root = root
    this.row = root
      .getByTestId('row')
      .filter({
        has: 'page' in root
          ? root.page().getByTestId('row name').first().getByText(title, { exact: true })
          : root.getByTestId('row name').first().getByText(title, { exact: true }),
      })
    this.name = this.row.getByTestId('row name').first()
    this.checkbox = this.row.getByRole('checkbox').first()
    this.menuButton = this.row.getByRole('button', { name: 'Actions' }).first()
    this.collapseButton = this.row.getByRole('button', { name: 'Next Actions' }).first()
  }

  async check() {
    await this.checkbox.check()
  }

  async uncheck() {
    await this.checkbox.uncheck()
  }

  async toggleCollapse() {
    await this.collapseButton.click()
  }

  private async openMenu() {
    await this.menuButton.click()
  }

  async openDetails() {
    await this.name.click()
  }

  async closeDetails() {
    await this.root.getByLabel('details close button').click()
  }

  async trash() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Trash' }).click()
  }

  async restore() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Restore' }).click()
  }

  async delete() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Delete' }).click()
  }

  private async openContextSelect() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Context' }).click()
  }

  private async openWaitingForSelect() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Waiting For' }).click()
  }

  private async openDueDate() {
    await this.openMenu()
    await this.root.getByRole('menuitem', { name: 'Due Date' }).click()
  }

  private async editSelect(select: Locator, value: string | null) {
    const page = select.page()

    await select.click()
    await select.press('Backspace')
    if (value) {
      await select.type(value)
      await select.press('ArrowDown')
      await select.press('Enter')
    }
    await page.getByRole('dialog').focus()
    await page.getByRole('button', { name: 'Save' }).click()
  }

  async edit(
    metadata: Partial<
      { context: string | null; waitingFor: string | null; dueDate: string; dueTime: string | null }
    >,
  ) {
    if (metadata.context !== undefined) {
      await this.openContextSelect()
      await this.editSelect(this.menuContextSelect, metadata.context)
    }
    if (metadata.waitingFor !== undefined) {
      await this.openWaitingForSelect()
      await this.editSelect(this.menuWaitingForSelect, metadata.waitingFor)
    }
    // calendar items must have a due date
    const submit = this.row.page().getByRole('button', { name: 'Save' })
    if (metadata.dueDate) {
      await this.openDueDate()
      await this.menuDateInput.fill(metadata.dueDate)
      await submit.focus()
      if (metadata.dueTime != null) {
        await this.menuTimeInput.type(metadata.dueTime)
      } else if (metadata.dueTime === null) {
        await this.menuTimeInput.click()
        await this.row.page().getByRole('button', { name: 'Clear' }).click()
      }
      await submit.focus()
      await submit.click()
    } else if (metadata.dueTime !== undefined) {
      await this.openDueDate()
      if (metadata.dueTime !== null) {
        await this.menuTimeInput.type(metadata.dueTime)
      } else {
        await this.menuTimeInput.click()
        await this.row.page().getByRole('button', { name: 'Clear' }).click()
      }
      await submit.focus()
      await submit.click()
    }
  }

  async process(
    dest: List,
    opts?: Partial<{ project: string; context: string; waitingFor: string; dueDate: string; dueTime: string }>,
  ) {
    await this.row.getByTestId('process button')
      .click()

    switch (dest) {
      case List.SomedayMaybe: {
        await this.root.locator('label').filter({ hasText: 'Someday/Maybe' }).click()
        break
      }
      case List.Reference: {
        await this.root.locator('label').filter({ hasText: 'References' }).click()
        break
      }
      case List.Project: {
        await this.root.getByLabel('Actionable').click()
        break
      }
      case List.DoIt: {
        await this.root.getByLabel('Actionable').click()
        await this.root.getByLabel('Single step to complete').click()
        break
      }
      case List.NextAction: {
        await this.root.getByLabel('Actionable').click()
        await this.root.getByLabel('Single step to complete').click()
        await this.root.getByLabel('More than 2 minutes').click()

        if (opts?.project) {
          const projectSelect = this.root.getByTestId('project select')
          await projectSelect.fill(opts.project)
          await projectSelect.press('ArrowDown')
          await projectSelect.press('Enter')
          await this.root.locator('#dest').focus()
        }
        if (opts?.context) {
          await this.contextSelect.fill(opts.context)
          await this.contextSelect.press('ArrowDown')
          await this.contextSelect.press('Enter')
          await this.root.locator('#dest').focus()
        }
        if (opts?.waitingFor) {
          await this.waitingForSelect.fill(opts.waitingFor)
          await this.waitingForSelect.press('ArrowDown')
          await this.waitingForSelect.press('Enter')
          await this.root.locator('#dest').focus()
        }

        break
      }
      case List.Calendar: {
        await this.root.getByLabel('Actionable').click()
        await this.root.getByLabel('Single step to complete').click()
        await this.root.getByLabel('More than 2 minutes').click()
        await this.root.getByLabel('Specific day/time').click()

        if (opts?.dueDate) {
          await this.dateInput.fill(opts.dueDate)
          await this.root.locator('#dest').focus()
        }
        if (opts?.dueTime) {
          await this.timeInput.type(opts.dueTime)
          await this.root.locator('#dest').focus()
        }

        break
      }
    }
    await expect(this.root.locator('#dest')).toHaveText(ListStr[dest])
    await this.root.locator('button:has-text("Submit")').click()
  }
}
