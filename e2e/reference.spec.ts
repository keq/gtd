import { test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test('References shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Reference)

  const titles = [Seed.Reference.A]

  await lp.expectTitles(titles)
})
