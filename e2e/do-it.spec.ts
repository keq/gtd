import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { List } from '~/lib/constants'
import { reseedTestUser } from 'prisma/seed'

test('Do-it modal works', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const title = await lp.addNonceToInbox()
  const row = lp.getRowByTitle(title)

  await row.process(List.DoIt)

  // make sure modal pops up and goes away once completed
  const doItModal = page.getByTestId('do it modal')
  await expect(doItModal).toHaveCount(1)
  await expect(doItModal.getByTestId('row name')).toHaveText(title)
  await row.check()
  await expect(doItModal).toHaveCount(0)

  await reseedTestUser()
})
