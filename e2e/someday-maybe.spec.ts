import { test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test('Someday/Maybe shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.SomedayMaybe)

  const titles = Object.values(Seed.SomedayMaybe)

  await lp.expectTitles(titles)
})
