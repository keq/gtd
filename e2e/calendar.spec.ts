import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

const onPage = Object.values(Seed.Calendar)

test('Calendar shows seeded data in chronological order', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Calendar)

  const rowNames = page.getByTestId('row name')
  await expect(rowNames).toHaveCount(onPage.length)
  const titles = await rowNames.allTextContents()

  const objectified = { ...titles }
  const correctOrder = { ...onPage }
  expect(objectified).toEqual(correctOrder)
})

test('Calendar shows correct relative date formats', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Calendar)

  await expect(lp.getRowByTitle(Seed.Calendar.K).row).toHaveText(/Yesterday .*/)
  await expect(lp.getRowByTitle(Seed.Calendar.M).row).toHaveText(/Today .*/)
  await expect(lp.getRowByTitle(Seed.Calendar.O).row).toHaveText(/Tomorrow .*/)
})

test('Due date and time can be changed', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Calendar)

  const in0 = { dueDate: '1030-01-01', dueTime: '1645' }
  const out0 = 'Jan 1, 1030 4:45 PM'
  const in1 = { dueDate: '1060-01-01', dueTime: '0700' }
  const out1 = 'Jan 1, 1060 7:00 AM'
  const in2 = { dueDate: '1060-01-01', dueTime: null }
  const out2 = 'Jan 1, 1060'

  const row = lp.getRowByTitle(Seed.Calendar.N)

  await row.edit(in0)
  await expect(row.row.getByText(out0, { exact: true })).toHaveCount(1)
  await page.reload()
  await expect(row.row.getByText(out0, { exact: true })).toHaveCount(1)

  await row.edit(in1)
  await expect(row.row.getByText(out1, { exact: true })).toHaveCount(1)
  await page.reload()
  await expect(row.row.getByText(out1, { exact: true })).toHaveCount(1)

  await row.edit(in2)
  await expect(row.row.getByText(out2, { exact: true })).toHaveCount(1)
  await page.reload()
  await expect(row.row.getByText(out2, { exact: true })).toHaveCount(1)
})
