import { expect, test } from '@playwright/test'

import { List, ListPath } from '~/lib/constants'

test.use({ storageState: '.emptyStorageState.json' })
test('homepage when logged out has hero header and link to Get Started', async ({ page }) => {
  await page.goto('/')

  await expect(page.getByText('Get Things Done')).toBeVisible()

  const signIn = page.getByText('Get Started')

  await signIn.click()

  await expect(page).toHaveURL('/api/auth/signin')
})

test('Inbox page when logged out redirects to hompage', async ({ page }) => {
  await page.goto(`/${ListPath[List.Inbox]}`)
  await expect(page).toHaveURL('/')
})
