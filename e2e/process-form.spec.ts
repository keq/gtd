import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { Seed , reseedTestUser } from 'prisma/seed'
import { List } from '~/lib/constants'

test.afterAll(async () => {
  await reseedTestUser()
})

test('process form sends to correct destination', async ({ page }) => {
  test.slow()

  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const dests = [
    List.SomedayMaybe,
    List.Reference,
    List.Project,
    List.NextAction,
    List.Calendar,
  ]
  for (const dest of dests) {
    const title = await lp.addNonceToInbox()
    const row = lp.getRowByTitle(title)
    await row.process(dest)

    await lp.expectTitles([title], { NOT: true })

    await lp.goto(dest)
    await lp.expectTitles([title])

    await lp.goto(List.Inbox)
  }
})

test("process form sends Next Action of Project to bottom of Project's Next Action list", async ({
  page,
}) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)
  const title = await lp.addNonceToInbox()
  const row = lp.getRowByTitle(title)
  const project = Seed.Project.A
  await row.process(List.NextAction, { project: project })

  // nonce should no longer be on the page
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Project)
  const projectRow = lp.getRowByTitle(project)

  // nonce should be the very last next action
  await expect(projectRow.row.getByTestId('row').last()).toHaveText(title)
})

test('process form correctly sets Next Action metadata', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)
  const title = await lp.addNonceToInbox()
  const row = lp.getRowByTitle(title)

  const project = Seed.Project.B
  const context = Seed.Context.A
  const waitingFor = Seed.WaitingFor.A

  await row.process(List.NextAction, { project, context, waitingFor })
  // nonce should no longer be on the page
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Project)

  // nonce should be on this page with correct metadata
  await lp.expectTitles([title])
  await expect(row.row.getByText(context, { exact: true })).toHaveCount(1)
  await expect(row.row.getByText(waitingFor, { exact: true })).toHaveCount(1)
})

test('process form correctly sets Calendar metadata', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)
  const title = await lp.addNonceToInbox()
  const row = lp.getRowByTitle(title)

  await row.process(List.Calendar, { dueDate: '1030-01-01', dueTime: '1530' })
  // nonce should no longer be on the page
  await lp.expectTitles([title], { NOT: true })

  await lp.goto(List.Calendar)

  // nonce should be on this page with correct metadata
  await lp.expectTitles([title])
  await expect(
    row.row.getByText('Jan 1, 1030 3:30 PM', { exact: true })
  ).toHaveCount(1)
})
