import { chromium, FullConfig } from '@playwright/test'

import { deleteAllTemporaryUsers } from 'prisma/seed'

async function globalSetup(config: FullConfig) {
  await deleteAllTemporaryUsers()
  const browser = await chromium.launch()
  // const browser = await chromium.launch({ headless: false, slowMo: 1000 })
  const page = await browser.newPage()

  // Save unauthenticated state to '.emptyStorageState.json'.
  await page.context().storageState({ path: '.emptyStorageState.json' })

  await page.goto(
    `${config.projects[0]?.use.baseURL ?? 'Playwright Config Error'}/api/auth/signin`,
  )
  await page.getByText('Sign in with Demo Account').click()

  // Save signed-in state to '.storageState.json'.
  await page.context().storageState({ path: '.storageState.json' })
  await browser.close()
}

export default globalSetup
