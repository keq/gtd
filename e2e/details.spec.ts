import { test } from '@playwright/test'

import { ListPage, nonce } from 'e2e/util'
import { Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test('Details: Things can be renamed', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const newTitle = nonce()
  const oldTitle = Seed.Inbox.A

  const row = lp.getRowByTitle(oldTitle)

  await row.openDetails()

  await lp.details.getByRole('textbox').fill(newTitle)
  await lp.expectTitles([newTitle])
  await lp.expectTitles([oldTitle], { NOT: true })

  await lp.details.getByRole('textbox').fill(oldTitle)
  await lp.expectTitles([newTitle], { NOT: true })
  await lp.expectTitles([oldTitle])
})

test('Details: completed next actions of project are shown', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Project)

  const row = lp.getRowByTitle(Seed.Project.A)
  await lp.expectTitles([Seed.Archive.PA_A], { NOT: true })
  await row.openDetails()
  await lp.expectTitles([Seed.Archive.PA_A])
})
