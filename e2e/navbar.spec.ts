import { expect, test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { List, ListStr } from '~/lib/constants'

test('Shows seeded counts', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const nav = page.getByRole('navigation')

  const cases = new Map([
    [List.Inbox, 3],
    [List.NextAction, 7],
    [List.Project, 2],
    [List.Calendar, 26],
    [List.Reference, 2],
    [List.SomedayMaybe, 2],
    [List.Archive, 3],
    [List.Trash, 6],
  ])

  for (
    const [list, count] of cases.entries()
  ) {
    const link = nav.getByRole('link', { name: ListStr[list] })
    await expect(link.getByText(count.toString())).toHaveCount(1)
  }
})
