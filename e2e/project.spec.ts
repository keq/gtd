import { expect, test } from '@playwright/test'

import { ListPage, Row } from 'e2e/util'
import { Seed , reseedTestUser } from 'prisma/seed'
import { List, ListPath } from '~/lib/constants'

test('Projects shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Project)

  const titles = [
    ...Object.values(Seed.Project),
    ...Object.values(Seed.NextAction.PA),
    ...Object.values(Seed.NextAction.PB),
  ]
  await lp.expectTitles(titles)
  await page.goto(`/${ListPath[List.Project]}`)
})

test('next actions adder is in Projects but not Next Actions', async ({
  page,
}) => {
  const lp = new ListPage(page)
  const add = page.getByTestId('add button')

  await lp.goto(List.NextAction)
  await expect(add).toHaveCount(0)

  await lp.goto(List.Project)
  await expect(add).toHaveCount(2)
})

test('adding Next Action to Project shows up immediately', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Project)
  const title = await lp.addNonceToProject(Seed.Project.A)
  await lp.expectTitles([title])
  await reseedTestUser()
})

test('checking/unchecking next action hides/unhides it immediately', async ({
  page,
}) => {
  const lp = new ListPage(page)
  await lp.goto(List.Project)

  const projTitle = Seed.Project.A
  const nATitle = Seed.NextAction.PA.A

  const row = lp.getRowByTitle(projTitle)
  await row.openDetails()

  const rowNA = new Row(row.row, nATitle)
  const detailsNA = new Row(lp.details, nATitle)

  await expect(rowNA.row).toBeVisible()
  await detailsNA.check()
  await expect(rowNA.row).not.toBeVisible()
  await detailsNA.uncheck()
  await expect(rowNA.row).toBeVisible()
})

test('collapsing/uncollapsing is immediate and persistent', async ({
  page,
}) => {
  const lp = new ListPage(page)

  const testCollapse = async (
    list: List,
    project: string,
    nextActions: string[]
  ) => {
    await lp.goto(list)

    const row = lp.getRowByTitle(project)
    await lp.expectTitles(nextActions)
    await row.toggleCollapse()
    await lp.expectTitles(nextActions, { NOT: true })

    await lp.goto(List.NextAction)
    await lp.goto(list)

    await lp.expectTitles(nextActions, { NOT: true })
    await row.toggleCollapse()
    await lp.expectTitles(nextActions)
  }

  await testCollapse(
    List.Project,
    Seed.Project.A,
    Object.values(Seed.NextAction.PA)
  )
  await testCollapse(List.Archive, Seed.Archive.P_A, [Seed.Archive.P_A_A])
})

test("Projects page's next actions do NOT show project pointers", async ({
  page,
}) => {
  const lp = new ListPage(page)
  await lp.goto(List.Project)
  await expect(page.getByTestId('project pointer')).toHaveCount(0)
})
