import { test } from '@playwright/test'

import { ListPage } from 'e2e/util'
import { reseedTestUser, Seed } from 'prisma/seed'
import { List } from '~/lib/constants'

test.afterAll(async () => {
  await reseedTestUser()
})

test('Inbox shows seeded data', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)

  const titles = Object.values(Seed.Inbox)
  await lp.expectTitles(titles)
})

test('adding to inbox shows up immediately', async ({ page }) => {
  const lp = new ListPage(page)
  await lp.goto(List.Inbox)
  const title = await lp.addNonceToInbox()
  await lp.expectTitles([title])
})
