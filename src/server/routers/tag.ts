import { TagType } from '@prisma/client'
import { z } from 'zod'

import { defaultTagSelect } from '~/server/defaultSelectors'
import { procedure, router } from '~/server/trpc'

const contextRouter = router({
  add: procedure
    .input(z.object({ id: z.string().cuid().optional(), type: z.nativeEnum(TagType), name: z.string() }))
    .mutation(async ({ ctx, input }) => {
      return await ctx.prisma.tag.create({
        data: {
          id: input.id,
          user: {
            connect: { email: ctx.user.email ?? undefined },
          },
          type: input.type,
          name: input.name,
        },
        select: defaultTagSelect,
      })
    }),
  all: procedure
    .input(z.object({ type: z.nativeEnum(TagType).optional() }))
    .query(async ({ ctx, input }) => {
      return await ctx.prisma.tag.findMany({
        where: {
          user: { email: ctx.user.email },
          type: input.type,
        },
        select: defaultTagSelect,
      })
    }),
})

export default contextRouter
