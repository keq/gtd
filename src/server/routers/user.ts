import { TRPCError } from '@trpc/server'

import { env } from '~/server/env'
import { procedure, router, unAuthedProcedure } from '~/server/trpc'

export const userRouter = router({
  webcalUrl: procedure.query(async ({ ctx }) => {
    const user = await ctx.prisma.user.findUnique({
      where: { email: ctx.user.email ?? undefined },
      select: { webcalId: true },
    })

    if (user === null) {
      throw new TRPCError({ code: 'UNAUTHORIZED' })
    }

    return `webcal://${
      env.NEXTAUTH_URL.replace(
        /^https?:\/\//,
        '',
      )
    }/api/calendar/${user.webcalId}`
  }),
  darkTheme: unAuthedProcedure.query(
    async ({ ctx }) => {
      const user = ctx.session?.user

      if (!user) {
        return false
      }
      const found = await ctx.prisma.user.findUnique(
        {
          where: { email: user.email ?? undefined },
          select: { darkTheme: true },
        },
      )

      if (found === null) {
        return false
      } else {
        return found.darkTheme
      }
    },
  ),
  toggleDarkTheme: procedure
    .mutation(async ({ ctx }) => {
      const user = await ctx.prisma.user.findUnique({
        where: { email: ctx.user.email ?? undefined },
        select: { darkTheme: true },
      })

      if (user === null) {
        throw new TRPCError({ code: 'PRECONDITION_FAILED' })
      }

      return await ctx.prisma.user.update({
        where: { email: ctx.user.email ?? undefined },
        data: { darkTheme: !user.darkTheme },
        select: { id: true },
      })
    }),
  temporary: procedure.query(async ({ ctx }) => {
    return (await ctx.prisma.user.findUniqueOrThrow({
      where: { email: ctx.user.email ?? undefined },
      select: { temporary: true },
    })).temporary
  }),
})
