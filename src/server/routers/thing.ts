import { Prisma, TagType } from '@prisma/client'
import { TRPCError } from '@trpc/server'
import { z } from 'zod'

import { List } from '~/lib/constants'
import { backToFront, frontToBack } from '~/lib/date'
import Lexico from '~/lib/lexico'
import { appendPos, prependPos } from '~/lib/pos'
import { defaultThingSelect } from '~/server/defaultSelectors'
import { procedure, router } from '~/server/trpc'

const computeWithoutInput = (
  thing: Prisma.ThingGetPayload<{ select: typeof defaultThingSelect }>,
) => {
  // add some computed fields that were frequently computed in various places in the frontend
  const { tags, dueDate, dueTime, ...rest } = thing
  const contexts = tags.flatMap(({ tag }) => (tag.type === 'Context' ? { id: tag.id, name: tag.name } : []))
  const waitingFors = tags.flatMap((
    { tag, timestamp },
  ) => (tag.type === 'WaitingFor' ? { id: tag.id, name: tag.name, timestamp } : []))

  const { dueDate: computedDueDate, dueTime: computedDueTime } = dueDate === null
    ? { dueDate: null, dueTime: null }
    : backToFront({ dueDate, dueTime })
  return {
    ...rest,
    contexts,
    waitingFors,
    onTrashPage: rest.trashed !== null || rest.project?.trashed != null,
    onArchivePage: rest.completed !== null || rest.project?.completed != null,
    dueDate: computedDueDate,
    dueTime: computedDueTime,
  }
}

/**
 * @param thing - Thing returned from query with default select
 * @param input - Input taken by thing.all (used to compute invalidate key)
 * @returns Thing with some fields removed and computed fields added
 */
const compute = (
  thing: Prisma.ThingGetPayload<{ select: typeof defaultThingSelect }>,
  input: z.infer<typeof thingAllInput>,
) => {
  return {
    ...computeWithoutInput(thing),
    // remove undefined fields because of https://github.com/TanStack/query/issues/3741
    invalidateKeys: 'projectId' in input && input.projectId !== undefined
      ? [input, { list: List.NextAction }]
      : thing.list === input.list
      ? [{ list: input.list }]
      : [{ list: input.list }, { list: thing.list }],
  }
}

const thingAllInput = z.discriminatedUnion('list', [
  z.object({
    list: z.enum([
      List.Inbox,
      List.SomedayMaybe,
      List.Reference,
      List.Project,
      List.DoIt,
      List.Calendar,
      List.Archive,
      List.Trash,
    ]),
  }),
  z.object({
    list: z.literal(List.NextAction),
    projectId: z.string().cuid().optional(),
    completed: z.null().optional(),
    tagIds: z.array(z.string().cuid()).optional(),
  }),
])

const thingRouter = router({
  all: procedure
    .input(thingAllInput)
    .query(async ({ ctx, input }) => {
      let res: Prisma.ThingGetPayload<{ select: typeof defaultThingSelect }>[]

      if (input.list === List.Archive) {
        res = await ctx.prisma.thing.findMany({
          where: {
            ...ctx.whereThing,
            completed: undefined,
            NOT: { completed: null },
            project: null,
          },
          orderBy: { completed: 'desc' },
          select: defaultThingSelect,
        })
      } else if (input.list === List.Trash) {
        res = await ctx.prisma.thing.findMany({
          where: {
            ...ctx.whereThing,
            completed: undefined,
            trashed: undefined,
            NOT: { trashed: null },
            project: null,
          },
          orderBy: { trashed: 'desc' },
          select: defaultThingSelect,
        })
      } else if (input.list === List.NextAction) {
        const common = {
          ...ctx.whereThing,
          list: List.NextAction,
          tags: input.tagIds && input.tagIds.length !== 0
            ? { some: { tag: { OR: input.tagIds.map((tagId) => ({ id: tagId })) } } }
            : undefined,
        }
        if (input.projectId !== undefined) {
          res = await ctx.prisma.thing.findMany({
            where: {
              ...common,
              projectId: input.projectId,
              completed: input.completed,
            },
            orderBy: { pos: 'asc' },
            select: defaultThingSelect,
          })
        } else {
          // nextActions for the Next Actions page must filter out:
          // 1. next actions that are NOT the first next action of a project
          // 2. next actions of completed or trashed projects
          const firstOfProj = await ctx.prisma.thing.findMany({
            where: {
              ...common,
              project: {
                completed: null,
                trashed: null,
              },
            },
            distinct: ['projectId'],
            orderBy: { pos: 'asc' },
            select: defaultThingSelect,
          })
          const independent = await ctx.prisma.thing.findMany({
            where: {
              ...common,
              projectId: null,
            },
            orderBy: { pos: 'asc' },
            select: defaultThingSelect,
          })

          res = firstOfProj.concat(independent)
        }
      } else {
        res = await ctx.prisma.thing.findMany({
          where: {
            ...ctx.whereThing,
            list: input.list,
          },
          orderBy: input.list === List.Calendar
            ? [{ dueDate: 'asc' }, { dueTime: 'asc' }, { pos: 'asc' }]
            : { pos: 'asc' },
          select: defaultThingSelect,
        })
      }

      return res.map((thing) => compute(thing, input))
    }),
  counts: procedure.query(async ({ ctx }) => {
    const count = async (list: List): Promise<[List, number]> => {
      let res: number
      if (list === List.Archive) {
        res = await ctx.prisma.thing.count({
          where: {
            ...ctx.whereThing,
            ...ctx.whereArchive,
          },
        })
      } else if (list === List.Trash) {
        res = await ctx.prisma.thing.count({
          where: {
            ...ctx.whereThing,
            ...ctx.whereTrash,
          },
        })
      } else if (list === List.NextAction) {
        res = Number(
          (await ctx.prisma.$queryRaw<[{ count: bigint }]>`with nextactions as (
    select things.name, things.pos, things.project_id from things join users on users.id = things.user_id where users.email = ${ctx.user.email} and things.list = 'NextAction' and things.completed is null and things.trashed is null order by pos asc
),
firsts as (
    select distinct on (nextactions.project_id) nextactions.name from nextactions left join things projects on projects.id = nextactions.project_id where nextactions.project_id is not null and projects.completed is null and projects.trashed is null order by nextactions.project_id, nextactions.pos asc
),
indies as (
    select nextactions.name from nextactions where project_id is null order by nextactions.pos asc
),
firsts_and_indies as (
    select firsts.name from firsts
    UNION ALL
    select indies.name from indies
)

select count(*) from firsts_and_indies;`)[0].count,
        )
      } else {
        res = await ctx.prisma.thing.count({
          where: {
            ...ctx.whereThing,
            list,
          },
        })
      }
      return [list, res]
    }
    const counts = Promise.all([
      List.Inbox,
      List.NextAction,
      List.Project,
      List.Calendar,
      List.Reference,
      List.SomedayMaybe,
      List.Archive,
      List.Trash,
    ].map((list) => count(list)))

    return new Map(await counts)
  }),
  byId: procedure
    .input(z.object({ id: z.string().cuid() }))
    .query(async ({ ctx, input }) => {
      return computeWithoutInput(
        await ctx.prisma.thing.findUniqueOrThrow({
          where: { id: input.id },
          select: defaultThingSelect,
        }),
      )
    }),
  add: procedure
    .input(
      z.discriminatedUnion('list', [
        z.object({
          id: z.string().cuid().optional(),
          list: z.literal(List.Inbox),
          name: z.string(),
        }),
        z.object({
          id: z.string().cuid().optional(),
          list: z.literal(List.NextAction),
          name: z.string(),
          projectId: z.string().cuid(),
          tagIds: z.array(z.string().cuid()).optional(),
        }),
      ]),
    )
    .mutation(async ({ ctx, input }) => {
      if (input.list === List.Inbox) {
        return await ctx.prisma.thing.create({
          data: {
            user: { connect: { email: ctx.user.email ?? undefined } },
            id: input.id,
            pos: await prependPos(ctx),
            list: input.list,
            name: input.name,
          },
          select: defaultThingSelect,
        })
      } else {
        return await ctx.prisma.thing.create({
          data: {
            user: { connect: { email: ctx.user.email ?? undefined } },
            id: input.id,
            pos: await appendPos(ctx),
            list: input.list,
            name: input.name,
            project: { connect: { id: input.projectId } },
            tags: input.tagIds ? { create: input.tagIds.map((tId) => ({ tag: { connect: { id: tId } } })) } : undefined,
          },
          select: defaultThingSelect,
        })
      }
    }),
  edit: procedure
    .input(
      z.object({
        id: z.string().cuid(),
        data: z
          .object({
            name: z.string(),
            note: z.string().nullable(),
            tags: z.record(
              z.nativeEnum(TagType),
              z.array(z.string().cuid()),
            ),
            dueDate: z.date().nullable(),
            dueTime: z.date().nullable(),
          })
          .partial(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { tags, dueDate, dueTime, ...rest } = input.data

      if (tags === undefined) {
        return await ctx.prisma.thing.update({
          where: { id: input.id },
          data: dueDate == null ? rest : { ...rest, ...frontToBack({ dueDate, dueTime: dueTime ?? null }) },
          select: defaultThingSelect,
        })
      } else {
        const deleteUnincludedTagsOfIncludedTagTypes = ctx.prisma.tagsOnThings.deleteMany({
          where: {
            thingId: input.id,
            tag: {
              OR: Object.entries(tags).map(([tagType, tagIds]) => ({
                type: tagType as TagType,
                NOT: { OR: tagIds.map((tagId) => ({ id: tagId })) },
              })),
            },
          },
        })
        const update = ctx.prisma.thing.update({
          where: { id: input.id },
          data: {
            ...rest,
            tags: {
              connectOrCreate: Object.values(tags).map((tagIds) =>
                tagIds.map((tagId) => ({
                  where: { thingId_tagId: { thingId: input.id, tagId: tagId } },
                  create: { tag: { connect: { id: tagId } } },
                }))
              ).flat(),
            },
          },
          select: defaultThingSelect,
        })
        return (await ctx.prisma.$transaction([deleteUnincludedTagsOfIncludedTagTypes, update]))[1]
      }
    }),
  delete: procedure
    .input(z.object({ id: z.string().cuid() }))
    .mutation(async ({ ctx, input }) => {
      return await ctx.prisma.thing.delete({
        where: { id: input.id },
        select: { name: true, list: true },
      })
    }),
  clearTrash: procedure.mutation(async ({ ctx }) => {
    return await ctx.prisma.thing.deleteMany({
      where: { user: { email: ctx.user.email }, NOT: { trashed: null } },
    })
  }),
  pos: procedure
    .input(
      z.object({
        id: z.string().cuid(),
        data: z
          .object({ before: z.string().cuid(), after: z.string().cuid() })
          .partial()
          .refine(
            (data) =>
              !(data.before !== undefined && data.after !== undefined)
              && (data.before || data.after),
            'Either `before` or `after` (not both) must be given',
          ),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const poses = (
        await ctx.prisma.thing.findMany({
          orderBy: { pos: 'asc' },
          select: { pos: true },
        })
      ).map(({ pos }) => pos)

      let newPos: string
      if (input.data.before !== undefined) {
        const beforeTarget = (await ctx.prisma.thing.findUnique({
          where: { id: input.data.before },
          select: { pos: true },
        }))?.pos
        if (!beforeTarget) {
          throw new TRPCError({ code: 'PRECONDITION_FAILED' })
        }
        const beforeTargetIdx = poses.indexOf(beforeTarget)
        const beforeTargetPredecessor = poses[beforeTargetIdx - 1]
        if (beforeTargetPredecessor !== undefined) {
          newPos = new Lexico(beforeTargetPredecessor)
            .between(beforeTarget)
            .toString()
        } else {
          newPos = new Lexico(beforeTarget).prev().toString()
        }
      } else {
        const afterTarget = (await ctx.prisma.thing.findUnique({
          where: { id: input.data.after },
          select: { pos: true },
        }))?.pos

        if (!afterTarget) {
          throw new TRPCError({ code: 'PRECONDITION_FAILED' })
        }
        const afterTargetIdx = poses.indexOf(afterTarget)
        const afterTargetSuccessor = poses[afterTargetIdx + 1]

        if (afterTargetSuccessor !== undefined) {
          newPos = new Lexico(afterTarget)
            .between(afterTargetSuccessor)
            .toString()
        } else {
          newPos = new Lexico(afterTarget).next().toString()
        }
      }

      return await ctx.prisma.thing.update({
        where: { id: input.id },
        data: { pos: newPos },
      })
    }),
  toggleCollapse: procedure
    .input(z.object({ id: z.string().cuid() }))
    .mutation(async ({ ctx, input }) => {
      const thing = await ctx.prisma.thing.findUnique({
        where: {
          id: input.id,
        },
        select: { collapsed: true },
      })

      if (!thing) {
        throw new TRPCError({ code: 'PRECONDITION_FAILED' })
      }

      return await ctx.prisma.thing.update({
        where: { id: input.id },
        data: { collapsed: !thing.collapsed },
        select: defaultThingSelect,
      })
    }),
  toggleComplete: procedure
    .input(z.object({ id: z.string().cuid() }))
    .mutation(async ({ ctx, input }) => {
      const thing = await ctx.prisma.thing.findUnique({
        where: {
          id: input.id,
        },
        select: { completed: true },
      })

      if (!thing) {
        throw new TRPCError({ code: 'PRECONDITION_FAILED' })
      }

      if (thing.completed === null) {
        return await ctx.prisma.thing.update({
          where: { id: input.id },
          data: { completed: new Date() },
          select: defaultThingSelect,
        })
      } else {
        return await ctx.prisma.thing.update({
          where: { id: input.id },
          data: { completed: null },
          select: defaultThingSelect,
        })
      }
    }),
  toggleTrash: procedure
    .input(z.object({ id: z.string().cuid() }))
    .mutation(async ({ ctx, input }) => {
      const thing = await ctx.prisma.thing.findUnique({
        where: {
          id: input.id,
        },
        select: { trashed: true },
      })

      if (!thing) {
        throw new TRPCError({ code: 'PRECONDITION_FAILED' })
      }

      if (thing.trashed === null) {
        return await ctx.prisma.thing.update({
          where: { id: input.id },
          data: { trashed: new Date() },
          select: defaultThingSelect,
        })
      } else {
        return await ctx.prisma.thing.update({
          where: { id: input.id },
          data: { trashed: null },
          select: defaultThingSelect,
        })
      }
    }),
  process: procedure
    .input(
      z.discriminatedUnion('dest', [
        z.object({
          id: z.string().cuid(),
          dest: z.enum([
            List.SomedayMaybe,
            List.Reference,
            List.Project,
            List.DoIt,
          ]),
        }),
        z.object({
          id: z.string().cuid(),
          dest: z.literal(List.NextAction),
          projectId: z.string().cuid().optional(),
          tagIds: z.array(z.string().cuid()).optional(),
        }),
        z.object({
          id: z.string().cuid(),
          dest: z.literal(List.Calendar),
          dueDate: z.date(),
          dueTime: z.date().nullable(),
        }),
      ]),
    )
    .mutation(async ({ ctx, input }) => {
      if (input.dest === List.Calendar) {
        const { dueDate, dueTime } = frontToBack({ dueDate: input.dueDate, dueTime: input.dueTime })
        return ctx.prisma.thing.update({
          where: { id: input.id },
          data: {
            list: List.Calendar,
            dueDate,
            dueTime,
          },
          select: defaultThingSelect,
        })
      }
      if (input.dest === List.NextAction) {
        // if processing into a project's next actions list, make sure it's added to the bottom of that list
        // don't calculate this by looking at only next actions since it must be unique to all of user's things
        return ctx.prisma.thing.update({
          where: { id: input.id },
          data: {
            list: List.NextAction,
            pos: input.projectId === undefined ? undefined : await appendPos(ctx),

            project: input.projectId === undefined
              ? undefined
              : { connect: { id: input.projectId } },
            tags: input.tagIds ? { create: input.tagIds.map((tId) => ({ tag: { connect: { id: tId } } })) } : undefined,
          },
          select: defaultThingSelect,
        })
      }
      return ctx.prisma.thing.update({
        where: { id: input.id },
        data: { list: input.dest },
        select: defaultThingSelect,
      })
    }),
})

export default thingRouter
