import tagRouter from '~/server/routers/tag'
import thingRouter from '~/server/routers/thing'
import { userRouter } from '~/server/routers/user'
import { procedure, router } from '~/server/trpc'

/**
 * Create your application's root router
 * If you want to use SSG, you need export this
 * @link https://trpc.io/docs/ssg
 * @link https://trpc.io/docs/router
 */
const appRouter = router({
  healthz: procedure.query(() => 'yay!'),
  thing: thingRouter,
  tag: tagRouter,
  user: userRouter,
})

export type AppRouter = typeof appRouter
export default appRouter
