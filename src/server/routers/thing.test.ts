/**
 * @jest-environment node
 */
import { describe, expect, it } from '@jest/globals'

import { seedDemoUser } from 'prisma/seed'
import { List } from '~/lib/constants'
import { createContextInner } from '~/server/context'
import appRouter from '~/server/routers/_app'

describe('calendar router', () => {
  it('stores and retrieves same date objects', async () => {
    const testUser = await seedDemoUser()
    const session = {
      user: { email: testUser.email },
      expires: '3030-12-30T00:00:00Z',
    }

    const ctx = createContextInner({ session })
    const caller = appRouter.createCaller(ctx)

    const date = '2000-11-23'
    const dueDate = new Date(`${date}T00:00:00`)
    const dueTime = new Date(`${date}T16:40:35`)

    const { id } = await caller.thing.add({
      list: List.Inbox,
      name: 'calendar thing',
    })
    await caller.thing.process({
      id: id,
      dest: List.Calendar,
      dueDate,
      dueTime,
    })

    const dbThing = await ctx.prisma.thing.findUnique({ where: { id }, select: { dueDate: true, dueTime: true } })
    expect(dbThing).not.toBeNull()
    expect(dbThing?.dueDate).toEqual(date)
    expect(dbThing?.dueTime).toStrictEqual(dueTime)

    const thing = await caller.thing.byId({ id: id })

    expect(thing.dueDate).toStrictEqual(dueDate)
    expect(thing.dueTime).toStrictEqual(dueTime)

    await caller.thing.delete({ id: id })
  })
})
