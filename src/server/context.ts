import * as trpc from '@trpc/server'
import * as trpcNext from '@trpc/server/adapters/next'
import { Session } from 'next-auth'
import { getServerSession } from 'next-auth/next'

import { prisma } from '~/lib/prisma'
import { authOptions } from '~/pages/api/auth/[...nextauth]'

import type { CreateNextContextOptions } from '@trpc/server/adapters/next'

interface CreateInnerContextOptions extends Partial<CreateNextContextOptions> {
  session: Session | null
}

/**
 * Inner function for `createContext` where we create the context.
 * This is useful for testing when we don't want to mock Next.js' request/response
 * @link https://github.com/trpc/trpc/discussions/1740#discussioncomment-2524022
 */
export const createContextInner = (opts: CreateInnerContextOptions) => {
  return { prisma, session: opts.session }
}

/**
 * Creates context for an incoming request
 * @link https://trpc.io/docs/context
 */
export const createContext = async (
  opts: trpcNext.CreateNextContextOptions
): Promise<Context> => {
  // for API-response caching see https://trpc.io/docs/caching

  const { req, res } = opts
  const session = await getServerSession(req, res, authOptions)

  return createContextInner({ session })
}

export type Context = trpc.inferAsyncReturnType<typeof createContextInner>
