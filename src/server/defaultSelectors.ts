import { Prisma } from '@prisma/client'

/**
 * Default selectors for DB tables.
 * It's important to always explicitly say which fields you want to return in order to not leak extra information
 * @see https://github.com/prisma/prisma/issues/9353
 */
export const defaultUserSelect = Prisma.validator<Prisma.UserSelect>()({
  id: true,
  name: true,
  email: true,
  image: true,
})

export const defaultTagSelect = Prisma.validator<Prisma.TagSelect>()({
  id: true,
  type: true,
  name: true,
})

export const defaultThingSelect = Prisma.validator<Prisma.ThingSelect>()({
  id: true,
  list: true,
  pos: true,
  name: true,
  note: true,
  completed: true,
  trashed: true,
  collapsed: true,
  outcome: true,
  tags: { select: { tag: { select: defaultTagSelect }, timestamp: true }, orderBy: { timestamp: 'asc' } },
  project: {
    select: {
      id: true,
      name: true,
      completed: true,
      trashed: true,
    },
  },
  projectId: true,
  dueDate: true,
  dueTime: true,
})
