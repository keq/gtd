/**
 * This is your entry point to setup the root configuration for tRPC on the server.
 * - `initTRPC` should only be used once per app.
 * - We export only the functionality that we use so we can enforce which base procedures should be used
 *
 * Learn how to create protected base procedures and other things below:
 * @see https://trpc.io/docs/v10/router
 * @see https://trpc.io/docs/v10/procedures
 */

import { initTRPC, TRPCError } from '@trpc/server'
import superjson from 'superjson'

import { Context } from '~/server/context'

const t = initTRPC.context<Context>().create({
  /**
   * @see https://trpc.io/docs/v10/data-transformers
   */
  transformer: superjson,
  /**
   * @see https://trpc.io/docs/v10/error-formatting
   */
  errorFormatter({ shape }) {
    return shape
  },
})

/**
 * Create a router
 * @see https://trpc.io/docs/v10/router
 */
export const router = t.router

/**
 * Create an unprotected procedure
 * @see https://trpc.io/docs/v10/procedures
 */
export const unAuthedProcedure = t.procedure

/**
 * @see https://trpc.io/docs/v10/middlewares
 */
export const middleware = t.middleware

/**
 * @see https://trpc.io/docs/v10/merging-routers
 */
export const mergeRouters = t.mergeRouters

const isAuthed = middleware(({ next, ctx }) => {
  const user = ctx.session?.user

  if (!user) {
    throw new TRPCError({ code: 'UNAUTHORIZED' })
  }

  return next({
    ctx: {
      user: {
        ...user,
        email: user.email,
      },
      whereThing: {
        user: { email: user.email },
        completed: null,
        trashed: null,
      },
      whereArchive: {
        completed: undefined,
        NOT: { completed: null },
        project: null,
      },
      whereTrash: {
        completed: undefined,
        trashed: undefined,
        NOT: { trashed: null },
        project: null,
      },
    },
  })
})

/**
 * Protected base procedure
 */
export const procedure = t.procedure.use(isAuthed)
