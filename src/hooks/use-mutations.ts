import cuid from 'cuid'
import { tail, takeRight } from 'lodash-es'

import type { RowThing } from '~/components/row'
import { List } from '~/lib/constants'
import notify from '~/lib/notify'
import { RouterInput, trpc } from '~/lib/trpc'
import type { MutationOptions, RouterOutput, RouterProcedure } from '~/lib/trpc'

type InvalidateKey = RouterInput['thing']['all']
interface Context {
  snapshots: Map<InvalidateKey, RouterOutput['thing']['all'] | undefined> | undefined
}
type Updater<T> = (prevData: RouterOutput['thing']['all'], mutatedThing: T) => RouterOutput['thing']['all']

const setterEdit = (
  prevThings: RouterOutput['thing']['all'],
  mutatedThing: Pick<RowThing, 'id'>,
) => {
  return prevThings.map((pT) => pT.id === mutatedThing.id ? { ...pT, ...mutatedThing } : pT)
}

const setterDelete = (
  prevThings: RouterOutput['thing']['all'],
  mutatedThing: Pick<RowThing, 'id'>,
) => {
  return prevThings.filter((pT) => pT.id !== mutatedThing.id)
}
export type Setter = (
  prevThings: RouterOutput['thing']['all'],
  mutatedThing: Pick<RowThing, 'id'>,
) => RouterOutput['thing']['all']

export const genMutationOptions = <
  T extends
    | RouterProcedure['thing']['edit']
    | RouterProcedure['thing']['pos']
    | RouterProcedure['thing']['toggleTrash']
    | RouterProcedure['thing']['delete']
    | RouterProcedure['thing']['toggleCollapse']
    | RouterProcedure['thing']['toggleComplete'],
>(
  invalidateKeys: InvalidateKey[],
  setter: Setter,
  thingName?: RowThing['name'],
  errorPresentParticiple?: string,
  successPastParticiple?: string,
): MutationOptions<
  T,
  Context
> => {
  const utils = trpc.useContext()

  return {
    onMutate: async (mutatedThing) => {
      if (mutatedThing === undefined) return
      const snapshots: NonNullable<Context['snapshots']> = new Map()

      for (const invalidateKey of invalidateKeys) {
        await utils.thing.all.cancel(invalidateKey)
        const prevThings = utils.thing.all.getData(invalidateKey)
        utils.thing.all.setData(
          invalidateKey,
          (prevThings) =>
            prevThings !== undefined
              ? setter(prevThings, mutatedThing)
              : undefined,
        )
        snapshots.set(invalidateKey, prevThings)
      }
      return { snapshots }
    },
    onError: (_err, _mutatedThing, context) => {
      if (context === undefined || context.snapshots === undefined) return
      for (const [invalidateKey, prevThings] of context.snapshots) {
        utils.thing.all.setData(
          invalidateKey,
          prevThings,
        )
      }
      if (errorPresentParticiple && thingName) {
        notify.error({ message: `Error ${errorPresentParticiple} "${thingName}"` })
      }
    },
    onSettled: (_data, _err, mutatedThing, _ctx) => {
      void utils.thing.counts.invalidate()
      if (mutatedThing === undefined) return
      void utils.thing.byId.invalidate({ id: mutatedThing.id })
      for (const invalidateKey of invalidateKeys) {
        void utils.thing.all.invalidate(invalidateKey)
      }
    },
    onSuccess: () => {
      if (successPastParticiple && thingName) {
        notify.success({ message: `Successfully ${successPastParticiple} "${thingName}"` })
      }
    },
  }
}

export const useMutateAdd = (
  { list, projectId }: { list: typeof List.Inbox; projectId?: never } | {
    list: typeof List.NextAction
    projectId: string
  },
) => {
  const invalidateKey = { list, projectId }
  const updater: Updater<RouterInput['thing']['add']> = (prevData, mutatedThing) => {
    const mockedThing = {
      id: cuid(),
      pos: '',
      note: null,
      completed: null,
      trashed: null,
      collapsed: false,
      outcome: null,
      project: null,
      projectId: null,
      dueDate: null,
      dueTime: null,
      contexts: [],
      waitingFors: [],
      invalidateKeys: [invalidateKey],
      onTrashPage: false,
      onArchivePage: false,
      ...mutatedThing,
    }
    return list === List.Inbox ? [mockedThing, ...prevData] : [...prevData, mockedThing]
  }

  const utils = trpc.useContext()
  return trpc.thing.add.useMutation(
    {
      onMutate: async (mutatedThing) => {
        await utils.thing.all.cancel(invalidateKey)
        utils.thing.all.setData(
          invalidateKey,
          (prevData) => prevData ? updater(prevData, mutatedThing) : undefined,
        )
      },
      onError: (_err, mutatedThing, _context) => {
        utils.thing.all.setData(
          invalidateKey,
          (prevData) => prevData ? list === List.Inbox ? tail(prevData) : takeRight(prevData) : undefined,
        )
        notify.error({ message: `Error adding "${mutatedThing.name}"` })
      },
      onSettled: () => {
        void utils.thing.all.invalidate(invalidateKey, undefined, { cancelRefetch: true })
      },
    },
  )
}

export const useMutatePos = (
  thing: Pick<RowThing, 'name' | 'invalidateKeys'>,
) => {
  return trpc.thing.pos.useMutation(
    genMutationOptions(
      thing.invalidateKeys,
      setterEdit,
    ),
  )
}

export const useMutateToggleComplete = (
  thing: Pick<RowThing, 'name' | 'invalidateKeys'>,
) => {
  return trpc.thing.toggleComplete.useMutation(
    genMutationOptions(
      [...thing.invalidateKeys, { list: List.Archive }],
      setterEdit,
      thing.name,
      'completing/uncompleting',
    ),
  )
}

export const useMutateToggleCollapse = (
  thing: Pick<RowThing, 'name' | 'invalidateKeys'>,
) => {
  return trpc.thing.toggleCollapse.useMutation(
    genMutationOptions(
      thing.invalidateKeys,
      setterEdit,
      thing.name,
      'editing',
    ),
  )
}

export const useMutateEdit = (
  thing: Pick<RowThing, 'name' | 'invalidateKeys'>,
) => {
  return trpc.thing.edit.useMutation(
    genMutationOptions(
      thing.invalidateKeys,
      setterEdit,
      thing.name,
      'editing',
    ),
  )
}

export const useMutateToggleTrash = (
  thing: Pick<RowThing, 'name' | 'trashed' | 'invalidateKeys'>,
) => {
  return trpc.thing.toggleTrash.useMutation(
    genMutationOptions(
      [...thing.invalidateKeys, { list: List.Trash }],
      setterDelete,
      thing.name,
      thing.trashed === null ? 'trashing' : 'restoring',
      thing.trashed === null ? 'trashed' : 'restored',
    ),
  )
}

export const useMutateDelete = (
  thing: Pick<RowThing, 'name'>,
) => {
  return trpc.thing.delete.useMutation(
    genMutationOptions(
      [{ list: List.Trash }],
      setterDelete,
      thing.name,
      'deleting',
      'deleted',
    ),
  )
}
