import { useContext } from 'react'

import { ShellContext, ShellDispatchContext } from '~/contexts/shell-context'
import { throwExpr } from '~/lib/util'

export const useShellDispatch = () => {
  return useContext(ShellDispatchContext) ?? throwExpr('Context not provided')
}

export const useShellState = () => {
  return useContext(ShellContext) ?? throwExpr('Context not provided')
}
