import { useEffect, useRef, useState } from 'react'

import { doNothing } from '~/lib/util'

export const useScroll = () => {
  const ref = useRef<HTMLDivElement>(null)
  const cleanUp = useRef<() => void>(doNothing)

  const [scrollTop, setScrollTop] = useState(0)

  const handleScroll = () => {
    setScrollTop(ref.current?.scrollTop ?? 0)
  }

  useEffect(() => {
    if (cleanUp.current !== doNothing) {
      cleanUp.current()
      cleanUp.current = doNothing
    }
    const ele = ref.current
    if (ele !== null) {
      ele.addEventListener('scroll', handleScroll, { passive: true })
      cleanUp.current = () => ele.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const setScroll = (val: number) => {
    const ele = ref.current
    if (ele !== null) {
      ele.scrollTop = val
    }
  }

  return { scrollRef: ref, scroll: scrollTop, setScroll }
}
