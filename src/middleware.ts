import { getToken } from 'next-auth/jwt'
import { NextResponse } from 'next/server'

import type { NextMiddleware } from 'next/server'

export const middleware: NextMiddleware = async (req) => {
  // redirect users if signed out unless it's landing page
  // redirect users to inbox from landing page if they're signed in
  const isAuthorized = await getToken({ req, secret: process.env.NEXTAUTH_SECRET }) !== null
  if (req.nextUrl.pathname === '/') {
    if (isAuthorized) {
      return NextResponse.rewrite(new URL('/inbox', req.url))
    }
  } else {
    if (!isAuthorized) {
      return NextResponse.redirect(new URL('/', req.url))
    }
  }
}

export const config = {
  matcher: [
    '/',
    '/inbox',
    '/someday-maybe',
    '/reference',
    '/project',
    '/do-it',
    '/next-action',
    '/calendar',
    '/trash',
    '/archive',
  ],
}
