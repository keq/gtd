import { createContext, Dispatch } from 'react'

import type { DetailsThing } from '~/components/details'
import { RowThing } from '~/components/row'
import { RowMenuThing } from '~/components/row/row-menu'

interface State {
  detailsThing: DetailsThing | null
  processThing: RowThing | null
  menuThing: RowMenuThing | null
  menuPos: { x: number; y: number }
}

type Payload = Partial<State> | ((state: State) => Partial<State>)

export const shellReducer = (state: State, payload: Payload) => {
  return { ...state, ...(typeof payload === 'function' ? payload(state) : payload) }
}

export const ShellContext = createContext<State | null>(null)
export const ShellDispatchContext = createContext<Dispatch<Payload> | null>(null)
