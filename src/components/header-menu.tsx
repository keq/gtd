import { ActionIcon, Avatar, Menu, Text, useMantineColorScheme } from '@mantine/core'
import { IconLogout, IconMoonStars, IconSun } from '@tabler/icons'
import { useSession } from 'next-auth/react'

import { trpc } from '~/lib/trpc'

export const HeaderMenu = () => {
  const { data: session } = useSession()
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { colorScheme, toggleColorScheme } = useMantineColorScheme()
  const dark = colorScheme === 'dark'
  const avatarName = session?.user?.name ? session.user.name[0] : ''

  const temporaryQuery = trpc.user.temporary.useQuery()
  return (
    <Menu
      shadow='xl'
      position='bottom-end'
      withinPortal
      styles={{ itemIcon: { width: '50px' }, itemLabel: { maxWidth: '280px' } }}
    >
      <Menu.Target>
        <ActionIcon title='User Settings'>
          <Avatar
            src={null}
            radius='xl'
          >
            {avatarName}
          </Avatar>
        </ActionIcon>
      </Menu.Target>

      <Menu.Dropdown>
        <Menu.Item
          icon={
            <Avatar
              src={null}
              size='lg'
              radius='xl'
            >
              {avatarName}
            </Avatar>
          }
        >
          <Text size='sm' weight={500} truncate>
            {session?.user?.name}
          </Text>
          <Text color='dimmed' size='xs' truncate>
            {session?.user?.email}
          </Text>
          {temporaryQuery.isSuccess && temporaryQuery.data
            && (
              <Text color='red' size='xs'>
                Temporary account
              </Text>
            )}
        </Menu.Item>
        <Menu.Item
          icon={dark ? <IconSun size={16} /> : <IconMoonStars size={16} />}
          color={dark ? 'yellow' : 'blue'}
          onClick={() => toggleColorScheme()}
        >
          {dark ? 'Light Theme' : 'Dark Theme'}
        </Menu.Item>
        <Menu.Item icon={<IconLogout size={16} />} component='a' href='/api/auth/signout'>
          Sign Out
        </Menu.Item>
      </Menu.Dropdown>
    </Menu>
  )
}
