import { createStyles } from '@mantine/core'

export const NAVBAR_WIDTH = 280

const useStyles = createStyles((theme) => ({
  navbar: {
    maxWidth: 0,
    flex: '0 0 auto',
    height: 'calc(100vh - var(--header-height))',
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    boxShadow: theme.shadows.xs,
    transition: 'left 200ms ease 0s, max-width 200ms ease 0s',

    [theme.fn.smallerThan('sm')]: {
      position: 'fixed',
      top: 'var(--header-height)',
      left: -NAVBAR_WIDTH,
    },

    overflowY: 'auto',
    zIndex: 9,
  },

  opened: {
    maxWidth: '300px',
    [theme.fn.smallerThan('sm')]: {
      maxWidth: NAVBAR_WIDTH,
      left: 0,
    },
  },

  navbarBg: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    transition: 'opacity 200ms ease 0s',
    zIndex: 8,
    [theme.fn.smallerThan('sm')]: {
      display: 'block',
      position: 'absolute',
      height: '100vh',
      inset: 0,
      visibility: 'visible',
      opacity: 1,
    },
  },

  closed: {
    '&': {
      visibility: 'hidden',
      opacity: '0 ',
    },
  },
}))

export default useStyles
