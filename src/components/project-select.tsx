import { Select, SelectProps } from '@mantine/core'
import NextError from 'next/error'
import { forwardRef } from 'react'

import { List } from '~/lib/constants'
import { trpc } from '~/lib/trpc'

export const ProjectSelect = forwardRef<HTMLInputElement, Partial<SelectProps>>(function ProjectSelect(
  props,
  ref,
) {
  const projectQuery = trpc.thing.all.useQuery({ list: List.Project })
  if (projectQuery.error) {
    return (
      <NextError
        title={projectQuery.error.message}
        statusCode={projectQuery.error.data?.httpStatus ?? 500}
      />
    )
  }
  if (projectQuery.status !== 'success') {
    return <>Loading...</>
  }
  const { data: things } = projectQuery

  return (
    <Select
      data-testid='project select'
      placeholder='Type in a Project'
      ref={ref}
      data={things
        .filter((thing) => thing.completed === null)
        .map((thing) => ({
          value: thing.id,
          label: thing.name,
        }))}
      label='Project'
      searchable
      clearable
      {...props}
    />
  )
})
