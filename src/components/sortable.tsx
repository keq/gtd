import {
  closestCenter,
  DndContext,
  DragEndEvent,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core'
import { arrayMove, SortableContext, sortableKeyboardCoordinates, verticalListSortingStrategy } from '@dnd-kit/sortable'
import { ReactElement, useEffect, useState } from 'react'

import { trpc } from '~/lib/trpc'

const Sortable = ({
  children,
}: {
  children: ReactElement<{ id: string }>[]
}) => {
  const [order, setOrder] = useState<string[]>(
    children.map((child) => child.props.id),
  )

  useEffect(() => {
    setOrder(children.map((child) => child.props.id))
  }, [children])

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  )

  const utils = trpc.useContext()
  const mutation = trpc.thing.pos.useMutation(
    {
      onSettled: () => {
        void utils.thing.all.invalidate(undefined, undefined, { cancelRefetch: true })
      },
    },
  )

  const handleDragEnd = (event: DragEndEvent) => {
    const { active, over } = event
    if (over && active.id !== over.id) {
      setOrder((order) => {
        const oldIndex = order.indexOf(String(active.id))
        const newIndex = order.indexOf(String(over.id))
        const newArray = arrayMove(order, oldIndex, newIndex)
        return newArray
      })
      const oldIndex = order.indexOf(String(active.id))
      const newIndex = order.indexOf(String(over.id))
      const newOrder = arrayMove(order, oldIndex, newIndex)
      if (newIndex === 0) {
        const successor = newOrder[newIndex + 1]
        mutation.mutate({ id: String(active.id), data: { before: successor } })
      } else {
        const predecessor = newOrder[newIndex - 1]
        mutation.mutate({ id: String(active.id), data: { after: predecessor } })
      }
    }
  }

  return (
    <DndContext
      id='0' // https://github.com/clauderic/dnd-kit/issues/926
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragEnd={handleDragEnd}
    >
      <SortableContext items={order} strategy={verticalListSortingStrategy}>
        {order.map((id) => children.find((child) => child.props.id === id))}
      </SortableContext>
    </DndContext>
  )
}

export default Sortable
