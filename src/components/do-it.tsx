import { Box, Modal, Title } from '@mantine/core'
import NextError from 'next/error'

import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'
import { trpc } from '~/lib/trpc'
import { doNothing } from '~/lib/util'

export const DoIt = () => {
  const thingsQuery = trpc.thing.all.useQuery({ list: List.DoIt })
  if (thingsQuery.error) {
    return (
      <NextError
        title={thingsQuery.error.message}
        statusCode={thingsQuery.error.data?.httpStatus ?? 500}
      />
    )
  }

  if (thingsQuery.status !== 'success') {
    return <>Loading...</>
  }
  const { data: things } = thingsQuery

  if (things.length === 0) return null

  return (
    <Modal
      data-testid='do it modal'
      opened={true}
      onClose={doNothing}
      title={<Title order={2}>Do It!</Title>}
      withCloseButton={false}
      closeOnEscape={false}
      closeOnClickOutside={false}
      trapFocus
    >
      <Box
        sx={(theme) => ({
          backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
        })}
        p='md'
      >
        <ListStack input={{ list: List.DoIt }} />
      </Box>
    </Modal>
  )
}
