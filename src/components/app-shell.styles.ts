import { createStyles } from '@mantine/core'

const useStyles = createStyles((theme) => ({
  main: {
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
    flex: '1 1 auto !important',
    height: 'calc(100vh - var(--header-height))',
    overflow: 'hidden',
  },
  container: {
    paddingTop: theme.spacing.md,
    paddingBottom: theme.spacing.md,
    paddingLeft: theme.spacing.xl * 1.5,
    paddingRight: theme.spacing.xl * 1.5,
  },
}))

export default useStyles
