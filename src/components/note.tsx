import { Stack, Text } from '@mantine/core'
import { useDebouncedState } from '@mantine/hooks'
import { RichTextEditor } from '@mantine/tiptap'
import { CodeBlockLowlight } from '@tiptap/extension-code-block-lowlight'
import { Placeholder } from '@tiptap/extension-placeholder'
import { useEditor } from '@tiptap/react'
import { StarterKit } from '@tiptap/starter-kit'
import { lowlight } from 'lowlight'
import { useEffect, useRef } from 'react'

import type { RowThing } from '~/components/row'
import { useMutateEdit } from '~/hooks/use-mutations'

const status = { 'error': 'error saving', 'idle': '', 'loading': '...saving', 'success': 'saved' } as const

export const Note = ({ thing }: { thing: Pick<RowThing, 'id' | 'name' | 'note' | 'invalidateKeys'> }) => {
  const editor = useEditor({
    extensions: [
      StarterKit,
      CodeBlockLowlight.configure({ lowlight }),
      Placeholder.configure({ placeholder: 'Start typing in a note' }),
    ],
    content: thing.note,
    onUpdate({ editor }) {
      setNote(editor.getHTML())
    },
  })

  const [note, setNote] = useDebouncedState(thing.note, 250)

  const didMount = useRef(false)
  const mutation = useMutateEdit(thing)
  useEffect(() => {
    // don't run on initial render
    if (!didMount.current) {
      didMount.current = true
      return
    }
    if (note === '' || note === '<p></p>') {
      mutation.mutate({ id: thing.id, data: { note: null } })
    } else {
      mutation.mutate({ id: thing.id, data: { note: note ?? undefined } })
    }
  }, [note]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Stack>
      <RichTextEditor editor={editor}>
        <RichTextEditor.Content />
      </RichTextEditor>
      <Text size='sm' color='dimmed' align='right'>
        {status[mutation.status]}
      </Text>
    </Stack>
  )
}
