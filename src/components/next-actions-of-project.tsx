import { Stack } from '@mantine/core'

import { ListStack } from '~/components/list-stack'
import { Add } from '~/components/row'
import type { RowThing } from '~/components/row'
import { List } from '~/lib/constants'

export const NextActionsOfProject = (
  { projectId, addable, completed }: {
    projectId: NonNullable<RowThing['projectId']>
    addable: boolean
    completed?: null
  },
) => {
  return (
    <Stack
      p='md'
      sx={(theme) => ({ backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0] })}
    >
      <ListStack
        data-testid='next actions of project'
        input={{ list: List.NextAction, projectId: projectId, completed: completed }}
      />
      {addable && <Add list={List.NextAction} projectId={projectId} />}
    </Stack>
  )
}
