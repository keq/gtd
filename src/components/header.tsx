import { ActionIcon, Burger, Group } from '@mantine/core'
import { IconHome } from '@tabler/icons'
import Link from 'next/link'
import React from 'react'

import { HeaderMenu } from '~/components/header-menu'
import useStyles from '~/components/header.styles'

export const Header = ({
  opened,
  setOpened,
}: {
  opened: boolean
  setOpened: React.Dispatch<React.SetStateAction<boolean>>
}) => {
  const { classes } = useStyles()

  return (
    <header className={classes.header}>
      <Group>
        <Burger
          sx={(theme) => ({
            '&>*,&>*::before,&>*::after': {
              backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.colors.dark[9],
            },
          })}
          title='Open navigation'
          opened={opened}
          onClick={() => setOpened((o) => !o)}
        />
        <Link href='/'>
          <ActionIcon color='dark'>
            <IconHome />
          </ActionIcon>
        </Link>
      </Group>
      <HeaderMenu />
    </header>
  )
}
