import { ActionIcon, Affix, Transition } from '@mantine/core'
import { IconArrowBarToUp } from '@tabler/icons'
import { ReactNode } from 'react'

import { useScroll } from '~/hooks/use-scroll'

/**
 * HOC to wrap given children in a scrollable div.
 * The HOC's parent must have a fixed height.
 */
export const Scrollable = ({ children }: { children: ReactNode }) => {
  const { scrollRef, scroll, setScroll } = useScroll()
  return (
    <div ref={scrollRef} style={{ height: '100%', overflowY: 'auto', scrollBehavior: 'smooth' }}>
      {children}
      <Affix position={{ bottom: 20, right: 20 }}>
        <Transition
          transition='slide-up'
          mounted={scroll > 0}
        >
          {(transitionStyles) => (
            <ActionIcon
              color='primary'
              radius='xl'
              size={64}
              variant='filled'
              style={transitionStyles}
              onClick={() => setScroll(0)}
            >
              <IconArrowBarToUp size={24} />
            </ActionIcon>
          )}
        </Transition>
      </Affix>
    </div>
  )
}
