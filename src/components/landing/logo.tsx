import { Group, Text } from '@mantine/core'
import Image from 'next/image'

const size = '24px'

export const Logo = () => {
  return (
    <Group spacing='xs'>
      <div style={{ overflow: 'hidden', borderRadius: '10%', width: size, height: size }}>
        <Image alt='app icon' width={size} height={size} src='/icon.svg' />
      </div>
      <Text fw={700} style={{ fontFamily: 'sans-serif' }}>Next of Thing</Text>
    </Group>
  )
}
