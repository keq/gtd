import { Box, Divider, Text } from '@mantine/core'

import { Logo } from '~/components/landing/logo'

export const Footer = () => {
  return (
    <Box
      sx={(theme) => ({
        display: 'flex',
        flexDirection: 'column',
        gap: theme.spacing.xs,
        position: 'sticky',
        bottom: 0,
        zIndex: 0,
        padding: theme.spacing.md,
      })}
    >
      <Logo />
      <Text c='dimmed'>Streamline your life with a modern Getting Things Done® implementation</Text>
      <Divider size='xs' />
      <Text c='dimmed'>Made by Daniel Koh</Text>
    </Box>
  )
}
