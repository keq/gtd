import { createStyles, keyframes } from '@mantine/core'

const BREAKPOINT = '@media (max-width: 755px)'
export const useStyles = createStyles((theme) => {
  const shine = keyframes({
    '0%': {
      backgroundPosition: '100% 50%',
    },
    '100%': {
      backgroundPosition: '-50% 50%',
    },
  })

  return {
    wrapper: {
      position: 'relative',
      boxSizing: 'border-box',
      backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.white,
    },

    inner: {
      position: 'relative',
      paddingTop: 100,
      paddingBottom: 100,

      [BREAKPOINT]: {
        paddingBottom: 80,
        paddingTop: 80,
      },
    },

    title: {
      fontFamily: `Greycliff CF, ${theme.fontFamily ?? 'sans-serif'}`,
      fontSize: 84,
      fontWeight: 900,
      lineHeight: 1.1,
      margin: 0,
      padding: 0,
      color: theme.colorScheme === 'dark' ? theme.white : theme.black,

      [BREAKPOINT]: {
        fontSize: 42,
        lineHeight: 1.2,
      },
    },

    description: {
      marginTop: theme.spacing.xl,
      fontSize: 24,

      [BREAKPOINT]: {
        fontSize: 18,
      },
    },

    controls: {
      marginTop: theme.spacing.xl * 2,

      [BREAKPOINT]: {
        marginTop: theme.spacing.xl,
      },
    },

    control: {
      height: 54,
      paddingLeft: 38,
      paddingRight: 38,

      [BREAKPOINT]: {
        height: 54,
        paddingLeft: 18,
        paddingRight: 18,
        flex: 1,
      },
    },

    control1: {
      background: theme.fn.linearGradient(90, theme.colors.blue[6], theme.colors.grape[6], theme.colors.blue[6]),
      backgroundSize: '300% 100%',
      animation: `${shine} 5s linear infinite`,
    },

    control2: {
      borderWidth: 2,
      borderColor: theme.colorScheme === 'dark' ? 'transparent' : theme.colors.dark[9],
    },

    feature: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',

      '@media (max-width: 800px)': {
        flexDirection: 'row',
      },
    },

    featureBody: {
      marginTop: theme.spacing.xs,

      '@media (max-width: 800px)': {
        marginTop: 0,
        marginLeft: theme.spacing.lg,
      },
    },

    featureTitle: {
      color: theme.colorScheme === 'dark' ? theme.white : theme.black,
    },
  }
})
