import { Box } from '@mantine/core'

import { Footer } from '~/components/landing/footer'
import { Header } from '~/components/landing/header'
import { JumboTron } from '~/components/landing/jumbotron'

export const Landing = () => {
  return (
    <>
      <Header />
      <Box sx={(theme) => ({ boxShadow: theme.shadows.md, position: 'relative', zIndex: 1, backgroundColor: 'white' })}>
        <JumboTron />
      </Box>
      <Footer />
    </>
  )
}
