import { Box, Button, Group } from '@mantine/core'

import { Logo } from '~/components/landing/logo'

export const Header = () => {
  return (
    <Box
      component='header'
      sx={(theme) => ({
        position: 'sticky',
        top: 0,
        zIndex: 2,
        backgroundColor: 'white',
        padding: theme.spacing.sm,
        paddingLeft: theme.spacing.xl,
        paddingRight: theme.spacing.xl,
      })}
    >
      <Group position='apart'>
        <Logo />
        <Button
          component='a'
          href='/api/auth/signin'
          variant='subtle'
        >
          Sign In
        </Button>
      </Group>
    </Box>
  )
}
