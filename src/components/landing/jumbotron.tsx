import { Button, Container, Group, SimpleGrid, Text, ThemeIcon, useMantineTheme } from '@mantine/core'
import { IconBolt, IconBook2, IconBrandGitlab, IconMoodSmileBeam } from '@tabler/icons'

import { useStyles } from '~/components/landing/jumbotron.styles'

const FEATURES = [
  {
    icon: IconBook2,
    title: 'Faithful to the book',
    description: 'No mixing of categories, no checkboxes on unactionable items, no excessive due dates',
  },
  {
    icon: IconBolt,
    title: 'Powerful',
    description: 'All of your Next Actions in one place and Calendar Actions synced to your calendar',
  },
  {
    icon: IconMoodSmileBeam,
    title: 'Simple',
    description: 'Minimal and easy to use',
  },
]

export const JumboTron = () => {
  const { classes, cx } = useStyles()
  const theme = useMantineTheme()

  const features = FEATURES.map((feature) => (
    <div className={classes.feature} key={feature.title}>
      <ThemeIcon
        size='xl'
        variant='gradient'
        gradient={{ from: 'blue', to: 'grape' }}
      >
        <feature.icon size={28} strokeWidth={1.5} />
      </ThemeIcon>

      <div className={classes.featureBody}>
        <Text weight={500} className={classes.featureTitle}>
          {feature.title}
        </Text>
        <Text size='sm' color='dimmed' mt={4}>
          {feature.description}
        </Text>
      </div>
    </div>
  ))

  return (
    <div className={classes.wrapper}>
      <Container size='lg' className={classes.inner}>
        <h1 className={classes.title}>Get Things Done</h1>

        <Text className={classes.description} color='dimmed'>
          Streamline your life with a modern Getting Things Done® implementation &mdash; Next of Thing has everything
          you need to adopt and master the workflow
        </Text>

        <SimpleGrid
          cols={3}
          sx={{ maxWidth: 800 }}
          spacing={30}
          mt={40}
          breakpoints={[{ maxWidth: 800, cols: 1 }]}
        >
          {features}
        </SimpleGrid>

        <Group className={classes.controls}>
          <Button
            component='a'
            href='https://gitlab.com/keq/nextofthing'
            size='xl'
            className={cx(classes.control, classes.control2)}
            color={theme.colorScheme === 'dark' ? 'gray' : 'dark'}
            leftIcon={<IconBrandGitlab />}
          >
            GitLab
          </Button>
          <Button
            component='a'
            href='/api/auth/signin'
            size='xl'
            className={cx(classes.control, classes.control1)}
          >
            Get started
          </Button>
        </Group>
      </Container>
    </div>
  )
}
