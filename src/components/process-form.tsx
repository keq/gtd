import { Box, Button, Checkbox, Collapse, Group, SegmentedControl, Stack, Text } from '@mantine/core'
import { DatePicker, TimeInput } from '@mantine/dates'
import { useForm } from '@mantine/form'
import { IconArrowBigDownLines } from '@tabler/icons'

import { ProjectSelect } from '~/components/project-select'
import { TagSelect } from '~/components/tag-select'
import { useShellDispatch } from '~/hooks/use-contexts'
import { List, ListStr } from '~/lib/constants'
import notify from '~/lib/notify'
import type { RouterOutput } from '~/lib/trpc'
import { trpc } from '~/lib/trpc'

interface FormValues {
  unActionableList: string
  isActionable: boolean
  isSingleStep: boolean
  moreThan2Min: boolean
  specificDate: boolean
  projectId: string | null
  contextIds: string[]
  waitingForIds: string[]
  dueDate: Date
  dueTime: Date | null
}

export const ProcessForm = ({
  thing,
}: {
  thing: Pick<RouterOutput['thing']['all'][number], 'id' | 'name' | 'list'>
}) => {
  if (!(thing.list === List.Inbox || thing.list === List.SomedayMaybe)) {
    throw Error(`${thing.list} is not ${List.Inbox} or ${List.SomedayMaybe}`)
  }

  const dispatch = useShellDispatch()

  const form = useForm<FormValues>({
    initialValues: {
      unActionableList: ListStr[
        List.SomedayMaybe
      ] as typeof ListStr[keyof typeof ListStr],
      isActionable: false,
      isSingleStep: false,
      moreThan2Min: false,
      specificDate: false,
      projectId: null,
      contextIds: [],
      waitingForIds: [],
      dueDate: new Date(),
      dueTime: null,
    },
  })

  let dest: Extract<
    List,
    | typeof List.SomedayMaybe
    | typeof List.Reference
    | typeof List.Project
    | typeof List.DoIt
    | typeof List.NextAction
    | typeof List.Calendar
  > = List.SomedayMaybe

  if (!form.values.isActionable) {
    switch (form.values.unActionableList) {
      case ListStr[List.SomedayMaybe]: {
        dest = List.SomedayMaybe
        break
      }
      case ListStr[List.Reference]: {
        dest = List.Reference
        break
      }
    }
  } else {
    if (!form.values.isSingleStep) {
      dest = List.Project
    } else {
      if (!form.values.moreThan2Min) {
        dest = List.DoIt
      } else {
        if (!form.values.specificDate) {
          dest = List.NextAction
        } else {
          dest = List.Calendar
        }
      }
    }
  }

  const utils = trpc.useContext()

  const mutation = trpc.thing.process.useMutation({
    onSuccess: () => {
      notify.success({
        message: `Moved "${thing.name}" to "${ListStr[dest]}"`,
      })
    },
    onError: (error) => {
      notify.error({ message: error.message })
    },
    onSettled: () => {
      void utils.thing.all.invalidate({ list: thing.list })
      // this should invalidate both standalone and project next actions
      void utils.thing.all.invalidate({ list: dest })
      void utils.thing.counts.invalidate()
    },
  })

  const handleSubmit = (values: FormValues) => {
    const { projectId, contextIds, waitingForIds, dueDate, dueTime } = values

    mutation.mutate({
      id: thing.id,
      dest: dest,
      projectId: projectId ?? undefined,
      tagIds: [...contextIds, ...waitingForIds],
      dueDate,
      dueTime,
    })

    dispatch({ processThing: null })
  }

  const unactionableDests = [
    ListStr[List.SomedayMaybe],
    ListStr[List.Reference],
  ]

  return (
    <Stack>
      <Stack align='center'>
        <Text>{thing.name}</Text>
        <IconArrowBigDownLines />
        <Text id='dest'>{ListStr[dest]}</Text>
      </Stack>
      <form onSubmit={form.onSubmit((values) => handleSubmit(values))}>
        <Stack spacing='xs'>
          <Collapse in={!form.values.isActionable}>
            <SegmentedControl
              fullWidth
              disabled={form.values.isActionable}
              data={unactionableDests}
              {...form.getInputProps('unActionableList')}
            />
          </Collapse>
          <Checkbox
            label='Actionable'
            {...form.getInputProps('isActionable')}
          />
          <Checkbox
            disabled={!form.values.isActionable}
            label='Single step to complete'
            {...form.getInputProps('isSingleStep')}
          />
          <Box>
            <Checkbox
              disabled={!form.values.isActionable || !form.values.isSingleStep}
              label='More than 2 minutes'
              {...form.getInputProps('moreThan2Min')}
            />
            <Collapse
              in={!(
                !form.values.isActionable
                || !form.values.isSingleStep
                || !form.values.moreThan2Min
                || form.values.specificDate
              )}
            >
              <ProjectSelect
                disabled={!form.values.isActionable
                  || !form.values.isSingleStep
                  || !form.values.moreThan2Min
                  || form.values.specificDate}
                {...form.getInputProps('projectId')}
              />
              <TagSelect
                tagType='Context'
                disabled={!form.values.isActionable
                  || !form.values.isSingleStep
                  || !form.values.moreThan2Min
                  || form.values.specificDate}
                {...form.getInputProps('contextIds')}
              />
              <TagSelect
                tagType='WaitingFor'
                disabled={!form.values.isActionable
                  || !form.values.isSingleStep
                  || !form.values.moreThan2Min
                  || form.values.specificDate}
                {...form.getInputProps('waitingForIds')}
              />
            </Collapse>
          </Box>
          <Box>
            <Checkbox
              disabled={!form.values.isActionable
                || !form.values.isSingleStep
                || !form.values.moreThan2Min}
              label='Specific day/time'
              {...form.getInputProps('specificDate')}
            />
            <Collapse
              in={!(
                !form.values.isActionable
                || !form.values.isSingleStep
                || !form.values.moreThan2Min
                || !form.values.specificDate
              )}
            >
              <DatePicker
                disabled={!form.values.isActionable
                  || !form.values.isSingleStep
                  || !form.values.moreThan2Min
                  || !form.values.specificDate}
                required
                allowFreeInput
                label='Date'
                {...form.getInputProps('dueDate')}
              />
              <TimeInput
                disabled={!form.values.isActionable
                  || !form.values.isSingleStep
                  || !form.values.moreThan2Min
                  || !form.values.specificDate}
                {...form.getInputProps('dueTime')}
                clearable
                clearButtonLabel='Clear'
                label='Time'
              />
            </Collapse>
          </Box>

          <Group position='right'>
            <Button type='submit'>Submit</Button>
          </Group>
        </Stack>
      </form>
    </Stack>
  )
}
