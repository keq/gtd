import { Button, Group, Skeleton, Stack, Textarea } from '@mantine/core'
import { useDebouncedValue } from '@mantine/hooks'
import { IconPlus } from '@tabler/icons'
import dynamic from 'next/dynamic'
import NextError from 'next/error'
import { useEffect, useRef, useState } from 'react'

import { NextActionsOfProject } from '~/components/next-actions-of-project'
import type { RowThing } from '~/components/row'
import { useMutateEdit } from '~/hooks/use-mutations'
import { List, ListIcon, ListStr } from '~/lib/constants'
import { trpc } from '~/lib/trpc'

const Note = dynamic(() => import('~/components/note').then((mod) => mod.Note), {
  loading: () => <Skeleton height={70} />,
})

export type DetailsThing = Pick<RowThing, 'id' | 'list' | 'name' | 'invalidateKeys'>

export const DetailsTitle = (
  { thing }: { thing: DetailsThing },
) => {
  const DetailsTitleIcon = ListIcon[thing.list]

  return (
    <Group spacing='xs' data-autofocus>
      <DetailsTitleIcon />
      {ListStr[thing.list]}
    </Group>
  )
}

const Name = ({ thing }: { thing: Pick<DetailsThing, 'id' | 'name' | 'invalidateKeys'> }) => {
  const [name, setName] = useState(thing.name)
  const [debouncedName] = useDebouncedValue(name, 250)

  const didMount = useRef(false)
  const mutation = useMutateEdit(thing)
  useEffect(() => {
    // don't run on initial render
    if (!didMount.current) {
      didMount.current = true
      return
    }
    mutation.mutate({ id: thing.id, data: { name: debouncedName } })
  }, [debouncedName]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Textarea
      size='xl'
      variant='unstyled'
      value={name}
      onChange={(e) => setName(e.currentTarget.value)}
      autosize
      maxRows={6}
    />
  )
}

export const Details = (
  { thing }: { thing: Pick<DetailsThing, 'id' | 'list' | 'invalidateKeys'> },
) => {
  const [hasNote, setHasNote] = useState(false)
  const thingQuery = trpc.thing.byId.useQuery({ id: thing.id })
  useEffect(() => {
    if (thingQuery.isSuccess) {
      setHasNote(thingQuery.data.note !== null && thingQuery.data.note.length !== 0)
    }
  }, [thingQuery.isSuccess, thingQuery.data])
  if (thingQuery.error) {
    return (
      <NextError
        title={thingQuery.error.message}
        statusCode={thingQuery.error.data?.httpStatus ?? 500}
      />
    )
  }

  if (thingQuery.status !== 'success') {
    return <Stack>{Array.from(Array(10).keys()).map((i) => <Skeleton key={i} height={60} />)}</Stack>
  }
  const { data: queriedThing } = thingQuery

  return (
    <Stack>
      <Name thing={{ ...queriedThing, invalidateKeys: thing.invalidateKeys }} />

      {hasNote
        ? <Note thing={{ ...queriedThing, invalidateKeys: thing.invalidateKeys }} />
        : <Button variant='subtle' leftIcon={<IconPlus size={14} />} onClick={() => setHasNote(true)}>Add Note</Button>}

      {thing.list === List.Project && (
        <NextActionsOfProject
          projectId={queriedThing.id}
          addable={queriedThing.trashed === null && queriedThing.completed === null}
        />
      )}
    </Stack>
  )
}
