import { Button } from '@mantine/core'
import { merge } from 'lodash-es'
import { ComponentProps } from 'react'

import type { ButtonProps } from '@mantine/core'

export const Badge = ({ children, ...others }: Omit<ComponentProps<'button'>, 'ref'> & ButtonProps) => {
  return (
    <Button
      size='xs'
      radius='xl'
      variant='subtle'
      compact
      {...others}
      styles={merge(others.styles, { leftIcon: { marginRight: 5 } })}
    >
      {children}
    </Button>
  )
}
