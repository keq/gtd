import { Button, Group, Paper, Stack, TextInput } from '@mantine/core'
import { useForm } from '@mantine/form'
import { useFocusTrap } from '@mantine/hooks'
import { IconPlus } from '@tabler/icons'
import cuid from 'cuid'
import { useState } from 'react'

import { useMutateAdd } from '~/hooks/use-mutations'
import { List } from '~/lib/constants'

export const Add = (
  { list, projectId }: { list: typeof List.Inbox; projectId?: never } | {
    list: typeof List.NextAction
    projectId: string
  },
) => {
  const [opened, setOpened] = useState(false)

  const focusTrapRef = useFocusTrap()

  const form = useForm({ initialValues: { name: '' } })

  const mutation = useMutateAdd(list === List.Inbox ? { list } : { list, projectId })

  const handleSubmit = ({ name }: { name: string }) => {
    form.reset()
    if (list === List.Inbox) {
      mutation.mutate({ id: cuid(), list, name })
    } else {
      mutation.mutate({ id: cuid(), list, name, projectId })
    }
  }

  const handleCancel = () => {
    setOpened(false)
    form.reset()
  }

  if (!opened) {
    return (
      <Button
        data-testid='add button'
        variant='subtle'
        leftIcon={<IconPlus size={24} />}
        onClick={() => setOpened(true)}
      >
        {list === List.Inbox ? 'Add to Inbox' : 'Add Next Action'}
      </Button>
    )
  } else {
    return (
      <Paper
        shadow='md'
        p='md'
        ref={focusTrapRef}
      >
        <form
          data-testid='add form'
          onSubmit={form.onSubmit((values) => handleSubmit(values))}
        >
          <Stack>
            <TextInput {...form.getInputProps('name')} />
            <Group position='right'>
              <Button variant='light' color='gray' onClick={handleCancel}>Cancel</Button>
              <Button type='submit' disabled={form.values.name.length === 0}>Add</Button>
            </Group>
          </Stack>
        </form>
      </Paper>
    )
  }
}
