import { createStyles } from '@mantine/core'

const useStyles = createStyles((theme) => ({
  row: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.md,
    gap: theme.spacing.md,
    overflow: 'visible',
    // https://stackoverflow.com/a/64486501
    // more ampersands means more specific lmao
    '&& *': {
      flexWrap: 'nowrap',
    },
    boxShadow: theme.shadows.xs,
    justifyContent: 'center',
    ':hover': {
      boxShadow: `0 0 0 2px ${theme.fn.variant({ variant: 'subtle' }).hover ?? ''}`,
    },
    cursor: 'pointer',
  },
  completed: {
    opacity: 0.5,
  },
  name: {
    overflow: 'auto',
    overflowWrap: 'break-word',
    '&&': {
      flexGrow: 1,
    },
  },
  chevron: {
    transition: 'transform 200ms ease',
    '&[data-rotate]': { transform: 'rotate(-90deg)' },
  },
}))

export default useStyles
