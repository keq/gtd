import { ActionIcon } from '@mantine/core'
import { IconSend } from '@tabler/icons'

import type { RowThing } from '~/components/row/row'
import { useShellDispatch } from '~/hooks/use-contexts'

const ProcessAction = ({
  thing,
}: {
  thing: RowThing
}) => {
  const dispatch = useShellDispatch()

  return (
    <ActionIcon
      data-testid='process button'
      onClick={() => dispatch({ processThing: thing })}
      title='Process'
      color='primary'
    >
      <IconSend />
    </ActionIcon>
  )
}

export default ProcessAction
