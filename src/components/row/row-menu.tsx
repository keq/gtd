import { Button, Group, Menu, MenuItemProps, Popover, Stack } from '@mantine/core'
import { DatePicker, TimeInput } from '@mantine/dates'
import { useForm } from '@mantine/form'
import {
  IconCalendarTime,
  IconChevronDown,
  IconCircleRectangle,
  IconHourglassHigh,
  IconListDetails,
  IconMapPin,
  IconTrash,
  IconTrashOff,
} from '@tabler/icons'
import { ComponentProps, FormEventHandler, ReactNode } from 'react'

import type { DetailsThing } from '~/components/details'
import type { RowThing } from '~/components/row'
import { TagSelect } from '~/components/tag-select'
import { useShellDispatch } from '~/hooks/use-contexts'
import { useMutateDelete, useMutateEdit, useMutateToggleTrash } from '~/hooks/use-mutations'
import { List } from '~/lib/constants'

type CustomMenuItemProps = Omit<ComponentProps<'button'>, 'ref'> & MenuItemProps

const WithPopoverForm = (
  { menuItem, children, onSubmit }:
    & { menuItem: ReactNode; children: ReactNode; onSubmit: FormEventHandler<HTMLFormElement> | undefined }
    & CustomMenuItemProps,
) => {
  const dispatch = useShellDispatch()

  return (
    <Popover offset={0} returnFocus>
      <Popover.Target>
        {menuItem}
      </Popover.Target>
      <Popover.Dropdown>
        <form
          onKeyDown={(e) => {
            if (e.key === 'ArrowUp' || e.key === 'ArrowDown') {
              e.preventDefault()
              e.stopPropagation()
            }
          }}
          onSubmit={(e) => {
            e.preventDefault()
            dispatch({ menuThing: null })
            if (onSubmit) onSubmit(e)
          }}
        >
          <Stack w={200}>
            {children}

            <Group grow>
              <Button
                color='gray'
                variant='outline'
                onClick={() => {
                  dispatch({ menuThing: null })
                }}
              >
                Cancel
              </Button>
              <Button type='submit'>Save</Button>
            </Group>
          </Stack>
        </form>
      </Popover.Dropdown>
    </Popover>
  )
}

const DeleteItem = ({ thing, ...others }: { thing: Pick<RowThing, 'id' | 'name'> } & CustomMenuItemProps) => {
  const delMutation = useMutateDelete(thing)
  return (
    <Menu.Item
      icon={<IconCircleRectangle size={14} />}
      color='red'
      onClick={() => delMutation.mutate({ id: thing.id })}
      {...others}
    >
      Delete Forever
    </Menu.Item>
  )
}

const TrashItem = ({
  thing,
  ...others
}: {
  thing: Pick<RowThing, 'id' | 'name' | 'trashed' | 'onTrashPage' | 'invalidateKeys'>
} & CustomMenuItemProps) => {
  const toggleTrashMutation = useMutateToggleTrash(thing)
  return (
    <Menu.Item
      icon={thing.onTrashPage ? <IconTrashOff size={14} /> : <IconTrash size={14} />}
      onClick={() => toggleTrashMutation.mutate({ id: thing.id })}
      {...others}
    >
      {thing.onTrashPage ? 'Restore' : 'Trash'}
    </Menu.Item>
  )
}

const ContextItem = ({
  thing,
  ...others
}: {
  thing: Pick<RowThing, 'id' | 'name' | 'contexts' | 'invalidateKeys'>
} & CustomMenuItemProps) => {
  const editMutation = useMutateEdit(thing)
  const form = useForm({
    initialValues: {
      contextIds: thing.contexts.map((context) => context.id),
    },
  })

  return (
    <WithPopoverForm
      menuItem={
        <Menu.Item
          icon={<IconMapPin size={14} />}
          rightSection={<IconChevronDown size={14} />}
          closeMenuOnClick={false}
          {...others}
        >
          Context
        </Menu.Item>
      }
      onSubmit={() =>
        editMutation.mutate({
          id: thing.id,
          data: {
            tags: { Context: form.values.contextIds },
          },
        })}
    >
      <TagSelect tagType='Context' {...form.getInputProps('contextIds')} />
    </WithPopoverForm>
  )
}

const WaitingForItem = ({
  thing,
  ...others
}: {
  thing: Pick<RowThing, 'id' | 'name' | 'waitingFors' | 'invalidateKeys'>
} & CustomMenuItemProps) => {
  const editMutation = useMutateEdit(thing)
  const form = useForm({
    initialValues: {
      waitingForIds: thing.waitingFors.map((waitingFor) => waitingFor.id),
    },
  })

  return (
    <WithPopoverForm
      menuItem={
        <Menu.Item
          icon={<IconHourglassHigh size={14} />}
          rightSection={<IconChevronDown size={14} />}
          closeMenuOnClick={false}
          {...others}
        >
          Waiting For
        </Menu.Item>
      }
      onSubmit={() =>
        editMutation.mutate({
          id: thing.id,
          data: {
            tags: { WaitingFor: form.values.waitingForIds },
          },
        })}
    >
      <TagSelect tagType='WaitingFor' {...form.getInputProps('waitingForIds')} />
    </WithPopoverForm>
  )
}

const DateTimeItem = ({
  thing,
  ...others
}: {
  thing: Pick<RowThing, 'id' | 'name' | 'waitingFors' | 'invalidateKeys' | 'dueDate' | 'dueTime'>
} & CustomMenuItemProps) => {
  const editMutation = useMutateEdit(thing)
  const form = useForm<{ dueDate: Date | null; dueTime: Date | null }>({
    initialValues: {
      dueDate: thing.dueDate,
      dueTime: thing.dueTime,
    },
  })

  return (
    <WithPopoverForm
      menuItem={
        <Menu.Item
          icon={<IconCalendarTime size={14} />}
          rightSection={<IconChevronDown size={14} />}
          closeMenuOnClick={false}
          {...others}
        >
          Due Date
        </Menu.Item>
      }
      onSubmit={() =>
        editMutation.mutate({
          id: thing.id,
          data: {
            dueDate: form.values.dueDate,
            dueTime: form.values.dueTime,
          },
        })}
    >
      <DatePicker
        required
        label='Date'
        allowFreeInput
        {...form.getInputProps('dueDate')}
      />
      <TimeInput
        {...form.getInputProps('dueTime')}
        clearable
        clearButtonLabel='Clear'
        label='Time'
      />
    </WithPopoverForm>
  )
}

export type RowMenuThing =
  & DetailsThing
  & Pick<
    RowThing,
    | 'id'
    | 'list'
    | 'trashed'
    | 'project'
    | 'contexts'
    | 'waitingFors'
    | 'invalidateKeys'
    | 'onTrashPage'
    | 'onArchivePage'
    | 'dueDate'
    | 'dueTime'
  >

export const RowMenu = ({ thing }: {
  thing: RowMenuThing
}) => {
  const dispatch = useShellDispatch()

  const isNotAnEditableNextAction = thing.list !== List.NextAction || thing.onTrashPage || thing.onArchivePage
  const isNotTrashable = thing.list === List.NextAction
    && (thing.project?.trashed != null || thing.project?.completed != null)
  return (
    <Menu.Dropdown>
      <Menu.Item icon={<IconListDetails size={14} />} onClick={() => dispatch({ detailsThing: thing })}>
        Details
      </Menu.Item>
      <Menu.Divider />
      <Menu.Label>Next Action</Menu.Label>
      <ContextItem thing={thing} disabled={isNotAnEditableNextAction} />
      <WaitingForItem thing={thing} disabled={isNotAnEditableNextAction} />
      <Menu.Divider />
      <Menu.Label>Calendar</Menu.Label>
      <DateTimeItem thing={thing} disabled={thing.list !== List.Calendar} />
      <Menu.Divider />
      <TrashItem data-testid='restore' thing={thing} disabled={isNotTrashable} />
      {thing.onTrashPage && <DeleteItem data-testid='delete' thing={thing} />}
    </Menu.Dropdown>
  )
}
