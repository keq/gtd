import { ActionIcon, Collapse, Group, Paper, Text } from '@mantine/core'
import { IconCalendarTime, IconChevronLeft, IconDots, IconHourglassHigh, IconMapPin, IconSubtask } from '@tabler/icons'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { isEqual } from 'lodash-es'
import React, { memo, MouseEvent, useEffect, useState } from 'react'

import { Badge } from '~/components/badge'
import { DetailsThing } from '~/components/details'
import { NextActionsOfProject } from '~/components/next-actions-of-project'
import CompleteAction from '~/components/row/complete-action'
import ProcessAction from '~/components/row/process-action'
import useStyles from '~/components/row/row.styles'
import { useShellDispatch } from '~/hooks/use-contexts'
import { useMutateToggleCollapse } from '~/hooks/use-mutations'
import { List, ListIcon } from '~/lib/constants'
import { fmt } from '~/lib/date'
import type { RouterOutput } from '~/lib/trpc'

import type { TagType } from '@prisma/client'

const htmlToPlain = (html: string) => (
  html
    .replace(/\n/ig, '')
    .replace(/<style[^>]*>[\s\S]*?<\/style[^>]*>/ig, '')
    .replace(/<head[^>]*>[\s\S]*?<\/head[^>]*>/ig, '')
    .replace(/<script[^>]*>[\s\S]*?<\/script[^>]*>/ig, '')
    .replace(/<\/\s*(?:p|div)>/ig, '\n')
    .replace(/<br[^>]*\/?>/ig, '\n')
    .replace(/<[^>]*>/ig, '')
    .replace('&nbsp;', ' ')
    .replace(/[^\S\r\n][^\S\r\n]+/ig, ' ')
)

dayjs.extend(relativeTime)

const TagBadge = ({ type, name, timestamp }: { type: TagType; name: string; timestamp?: Date }) => {
  return (
    <Badge
      leftIcon={type === 'Context' ? <IconMapPin size={14} /> : <IconHourglassHigh size={14} />}
      right={timestamp ? `(${dayjs(timestamp).fromNow()})` : undefined}
      title={timestamp?.toLocaleString()}
    >
      {name}
    </Badge>
  )
}

const ProjectPointer = ({ projectThing }: { projectThing: DetailsThing }) => {
  const dispatch = useShellDispatch()
  const ProjectIcon = ListIcon[List.Project]
  return (
    <Badge
      data-testid='project pointer'
      leftIcon={<ProjectIcon size={16} />}
      onClick={() => dispatch({ detailsThing: projectThing })}
    >
      {projectThing.name}
    </Badge>
  )
}

export type RowThing = Pick<
  RouterOutput['thing']['all'][number],
  | 'id'
  | 'list'
  | 'name'
  | 'note'
  | 'completed'
  | 'trashed'
  | 'collapsed'
  | 'project'
  | 'projectId'
  | 'contexts'
  | 'waitingFors'
  | 'dueDate'
  | 'dueTime'
  | 'invalidateKeys'
  | 'onTrashPage'
  | 'onArchivePage'
>

export const Row = memo(
  function Row(
    { thing }: { thing: RowThing },
  ) {
    const [collapsed, setCollapsed] = useState(thing.collapsed)
    const { classes, cx } = useStyles()

    const toggleCollapseMutation = useMutateToggleCollapse(thing)

    useEffect(() => {
      setCollapsed(thing.collapsed)
    }, [thing.collapsed])

    const dispatch = useShellDispatch()

    const openMenu = (e: MouseEvent<HTMLElement>) => {
      dispatch({ menuThing: thing, menuPos: { x: e.clientX, y: e.clientY } })
    }
    const openDetails = () => dispatch({ detailsThing: thing })
    const openDetailsOnSpecificClick = {
      onClick: (e: MouseEvent) => {
        if (e.target !== e.currentTarget) return
        openDetails()
      },
    }

    const processables = [List.Inbox, List.SomedayMaybe] as List[]
    const completables = [List.Project, List.DoIt, List.NextAction] as List[]
    const completed = Boolean(thing.completed)

    const left = thing.onTrashPage
      ? null
      : processables.includes(thing.list)
      ? <ProcessAction thing={thing} />
      : completables.includes(thing.list)
      ? <CompleteAction thing={thing} />
      : null
    const right = (
      <Group spacing={0}>
        {thing.list === List.Project && (
          <ActionIcon
            title='Next Actions'
            onClick={() => {
              setCollapsed((o) => !o)
              toggleCollapseMutation.mutate({ id: thing.id })
            }}
            sx={(theme) => ({ width: theme.spacing.xl * 2 })}
          >
            <IconSubtask />
            <IconChevronLeft
              data-rotate={!collapsed || undefined}
              className={classes.chevron}
            />
          </ActionIcon>
        )}
        <ActionIcon
          title='Actions'
          onClick={(e) => openMenu(e)}
        >
          <IconDots />
        </ActionIcon>
      </Group>
    )

    const projectPointer = thing.invalidateKeys.find((iK) => 'projectId' in iK) === undefined && thing.project
      ? (
        <ProjectPointer
          projectThing={{ ...thing.project, list: List.Project, invalidateKeys: thing.invalidateKeys }}
        />
      )
      : null

    const contextBadges = thing.contexts.map((context) => (
      <TagBadge key={context.name} type='Context' name={context.name} />
    ))
    const waitingForBadges = thing.waitingFors.map((waitingFor) => (
      <TagBadge key={waitingFor.name} type='WaitingFor' name={waitingFor.name} timestamp={waitingFor.timestamp} />
    ))

    const dueDateTimeBadge = thing.dueDate !== null
      ? (
        <Badge
          color='red'
          leftIcon={<IconCalendarTime size={14} />}
          title={fmt(3, thing.dueDate, thing.dueTime)}
        >
          {fmt(1, thing.dueDate, thing.dueTime)}
        </Badge>
      )
      : null

    return (
      <Paper
        className={completed ? cx(classes.row, classes.completed) : classes.row}
        data-testid='row'
        {...openDetailsOnSpecificClick}
        onContextMenu={(e) => {
          if (!e.defaultPrevented) {
            openMenu(e)
          }
          e.preventDefault()
        }}
      >
        <Group
          spacing='xs'
          position='apart'
          align='start'
        >
          {left}
          <span
            data-testid='row name'
            className={classes.name}
            title='Details'
            {...openDetailsOnSpecificClick}
          >
            {thing.name}
          </span>
          {right}
        </Group>
        {thing.note !== null
          && (
            <Text
              title='Note'
              c='dimmed'
              truncate
              {...openDetailsOnSpecificClick}
            >
              {htmlToPlain(thing.note).substring(0, 120)}
            </Text>
          )}
        <Group sx={{ flexWrap: 'wrap', ':empty': { display: 'none' } }} {...openDetailsOnSpecificClick}>
          {dueDateTimeBadge}
          {projectPointer}
          {contextBadges}
          {waitingForBadges}
        </Group>
        {thing.list === List.Project && (
          <Collapse
            in={!collapsed}
            p='xs'
            style={{ cursor: 'auto' }}
          >
            <NextActionsOfProject
              projectId={thing.id}
              addable={thing.trashed === null && thing.completed === null}
              completed={null}
            />
          </Collapse>
        )}
      </Paper>
    )
  },
  ({ thing: prevThing }, { thing: newThing }) => isEqual(prevThing, newThing),
)
