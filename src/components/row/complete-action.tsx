import { Checkbox } from '@mantine/core'
import { ChangeEvent, useEffect, useState } from 'react'

import type { RowThing } from '~/components/row'
import { useMutateToggleComplete } from '~/hooks/use-mutations'

const CompleteAction = ({
  thing,
}: {
  thing: Pick<RowThing, 'id' | 'name' | 'completed' | 'invalidateKeys'>
}) => {
  const [checked, setChecked] = useState(false)

  useEffect(() => {
    setChecked(Boolean(thing.completed))
  }, [thing.completed])

  const toggledMutation = useMutateToggleComplete(thing)

  const handleCheck = (evt: ChangeEvent<HTMLInputElement>) => {
    setChecked(evt.currentTarget.checked)
    toggledMutation.mutate({ id: thing.id })
  }
  return (
    <Checkbox
      sx={{ lineHeight: 0 }}
      size='md'
      checked={checked}
      onChange={handleCheck}
      aria-label={thing.name}
    />
  )
}

export default CompleteAction
