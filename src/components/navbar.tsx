import { NavLink, Text } from '@mantine/core'
import Link from 'next/link'
import React from 'react'

import useStyles from '~/components/navbar.styles'
import { List, ListIcon, ListPath, ListStr } from '~/lib/constants'
import { trpc } from '~/lib/trpc'

const links = new Map(
  [
    List.Inbox,
    List.NextAction,
    List.Project,
    List.Calendar,
    List.Reference,
    List.SomedayMaybe,
    List.Archive,
    List.Trash,
  ].map((list) => [
    list,
    {
      href: ListPath[list],
      text: ListStr[list],
      icon: ListIcon[list],
    },
  ]),
)

export const Navbar = ({
  opened,
  setOpened,
  path,
}: {
  opened: boolean
  setOpened: React.Dispatch<React.SetStateAction<boolean>>
  path: ListPath
}) => {
  const { classes, cx } = useStyles()
  const countsQuery = trpc.thing.counts.useQuery()
  const count = (list: List) => {
    if (countsQuery.isSuccess) {
      const { data: counts } = countsQuery
      const count_ = counts.get(list)
      if (count_ && count_ > 0) {
        return <Text c='dimmed'>{count_}</Text>
      }
    }
    return null
  }

  return (
    <>
      <nav className={cx(classes.navbar, { [classes.opened]: opened })}>
        {Array.from(links, ([list, link]) => (
          <Link key={link.href} href={link.href} passHref>
            <NavLink
              onClick={() => setOpened(false)}
              aria-label={link.text}
              component='a'
              label={<Text size='md'>{link.text}</Text>}
              icon={<link.icon stroke={1.5} />}
              rightSection={count(list)}
              active={link.href === path}
              noWrap
              pl='lg'
              pr='lg'
            />
          </Link>
        ))}
      </nav>
      <div
        className={cx(classes.navbarBg, { [classes.closed]: !opened })}
        onClick={() => setOpened(false)}
      >
      </div>
    </>
  )
}
