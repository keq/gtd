import { ActionIcon, CopyButton, Spoiler, TextInput, Tooltip } from '@mantine/core'
import { IconCheck, IconCopy } from '@tabler/icons'
import NextError from 'next/error'

import { trpc } from '~/lib/trpc'

const WebcalLink = () => {
  const webcalUrlQuery = trpc.user.webcalUrl.useQuery()
  if (webcalUrlQuery.error) {
    return (
      <NextError
        title={webcalUrlQuery.error.message}
        statusCode={webcalUrlQuery.error.data?.httpStatus ?? 500}
      />
    )
  }

  if (webcalUrlQuery.status !== 'success') {
    return <>Loading...</>
  }
  const { data: webcalUrl } = webcalUrlQuery

  const copyButton = (
    <CopyButton value={webcalUrl} timeout={2000}>
      {({ copied, copy }) => (
        <Tooltip label={copied ? 'Copied' : 'Copy'} withArrow position='right'>
          <ActionIcon color={copied ? 'teal' : 'gray'} onClick={copy}>
            {copied ? <IconCheck size={16} /> : <IconCopy size={16} />}
          </ActionIcon>
        </Tooltip>
      )}
    </CopyButton>
  )

  return (
    <Spoiler maxHeight={0} showLabel='Show Webcal URL' hideLabel='Hide'>
      <TextInput
        label='Add this URL to your Google Calendar/Outlook/etc.'
        value={webcalUrl}
        variant='filled'
        rightSection={copyButton}
        onFocus={(evt) => evt.target.select()}
      />
    </Spoiler>
  )
}

export default WebcalLink
