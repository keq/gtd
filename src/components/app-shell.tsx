import { Container, Group, Menu, Skeleton, Stack } from '@mantine/core'
import { useLocalStorage, useMediaQuery } from '@mantine/hooks'
import dynamic from 'next/dynamic'
import React, { ReactNode, useEffect, useReducer, useState } from 'react'

import useStyles from '~/components/app-shell.styles'
import { DoIt } from '~/components/do-it'
import { Header } from '~/components/header'
import { Heading } from '~/components/heading'
import { Navbar } from '~/components/navbar'
import { Scrollable } from '~/components/scrollable'
import { ShellContext, ShellDispatchContext, shellReducer } from '~/contexts/shell-context'
import { ListPath, PathList } from '~/lib/constants'

const Details = dynamic(() => import('~/components/details').then((mod) => mod.Details))
const DetailsTitle = dynamic(() => import('~/components/details').then((mod) => mod.DetailsTitle))
const Drawer = dynamic(() => import('@mantine/core').then((mod) => mod.Drawer))

const ProcessForm = dynamic(() => import('~/components/process-form').then((mod) => mod.ProcessForm), {
  loading: () => <Skeleton height={330} />,
})
const Modal = dynamic(() => import('@mantine/core').then((mod) => mod.Modal))

const RowMenu = dynamic(() => import('~/components/row/row-menu').then((mod) => mod.RowMenu), {
  loading: () => (
    <Menu.Dropdown>
      <Stack spacing={6} p={7}>
        {[...Array(5).keys()].map((i) => (
          <Group key={i} noWrap spacing={5}>
            <Skeleton height={22} width={22} />
            <Skeleton height={30} />
          </Group>
        ))}
      </Stack>
    </Menu.Dropdown>
  ),
})

const Provider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(shellReducer, {
    detailsThing: null,
    processThing: null,
    menuThing: null,
    menuPos: { x: 0, y: 0 },
  })

  return (
    <ShellContext.Provider value={state}>
      <ShellDispatchContext.Provider value={dispatch}>
        {children}

        <Menu
          opened={state.menuThing !== null}
          onChange={() => dispatch({ menuThing: null })}
          position='right-start'
          offset={0}
          shadow='xl'
          width={150}
          withinPortal
        >
          <Menu.Target>
            <div
              style={{
                position: 'fixed',
                left: state.menuPos.x,
                top: state.menuPos.y,
              }}
            />
          </Menu.Target>
          {state.menuThing !== null && <RowMenu thing={state.menuThing} />}
        </Menu>

        <Drawer
          data-testid='details'
          closeButtonLabel='details close button'
          opened={state.detailsThing !== null}
          onClose={() => dispatch({ detailsThing: null })}
          title={state.detailsThing ? <DetailsTitle thing={state.detailsThing} /> : undefined}
          position='right'
          size='xl'
          styles={(theme) => ({
            drawer: {
              display: 'flex',
              flexDirection: 'column',
              '&>*': {
                paddingTop: theme.spacing.xl,
                paddingBottom: theme.spacing.sm,
                paddingLeft: theme.spacing.xl,
                paddingRight: theme.spacing.xl,
                margin: 0,
              },
            },
            body: {
              paddingTop: theme.spacing.sm,
              paddingBottom: theme.spacing.xl,
              flexGrow: 1,
              overflowY: 'auto',
            },
          })}
        >
          {state.detailsThing !== null && <Details thing={state.detailsThing} />}
        </Drawer>

        <Modal
          opened={state.processThing !== null}
          onClose={() => {
            dispatch({ processThing: null })
          }}
          title='Process'
        >
          {state.processThing !== null && <ProcessForm thing={state.processThing} />}
        </Modal>
      </ShellDispatchContext.Provider>
    </ShellContext.Provider>
  )
}

export const AppShell = ({
  path,
  children,
}: {
  path: ListPath
  children: ReactNode
}) => {
  const [navbarOpenedWide, setNavbarOpenedWide] = useLocalStorage({ key: 'navbarOpenedWide', defaultValue: false })
  const [navbarOpenedNarrow, setNavbarOpenedNarrow] = useState(false)
  const narrow = useMediaQuery('(max-width: 768px)')

  useEffect(() => {
    if (!narrow) {
      setNavbarOpenedNarrow(false)
    }
  }, [narrow])

  const { classes } = useStyles()

  return (
    <Provider>
      <Stack spacing={0}>
        <Header
          opened={narrow ? navbarOpenedNarrow : false}
          setOpened={narrow ? setNavbarOpenedNarrow : setNavbarOpenedWide}
        />
        <Group align='stretch' noWrap spacing={0}>
          <Navbar
            opened={narrow ? navbarOpenedNarrow : navbarOpenedWide}
            setOpened={setNavbarOpenedNarrow}
            path={path}
          />
          <main className={classes.main}>
            <Scrollable>
              <Container className={classes.container}>
                <Heading list={PathList[path]} />
                {children}
              </Container>
            </Scrollable>
          </main>
        </Group>
      </Stack>

      <DoIt />
    </Provider>
  )
}
