import { Skeleton, Stack } from '@mantine/core'
import NextError from 'next/error'

import { ThingsStack } from '~/components/things-stack'
import { List } from '~/lib/constants'
import { RouterInput, trpc } from '~/lib/trpc'

export const ListStack = ({
  input,
}: { input: RouterInput['thing']['all'] }) => {
  const { list } = input

  const thingsQuery = trpc.thing.all.useQuery(input)
  if (thingsQuery.error) {
    return (
      <NextError
        title={thingsQuery.error.message}
        statusCode={thingsQuery.error.data?.httpStatus ?? 500}
      />
    )
  }

  if (thingsQuery.status !== 'success') {
    return <Stack>{Array.from(Array(10).keys()).map((i) => <Skeleton key={i} height={60} />)}</Stack>
  }
  const { data: things } = thingsQuery

  if (things.length === 0) return null
  return (
    <ThingsStack
      things={things}
      sortable={!([List.Trash, List.Archive, List.DoIt, List.Calendar] as List[]).includes(list)}
    />
  )
}
