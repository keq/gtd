import { ActionIcon, Group, Title } from '@mantine/core'
import { IconTrashX } from '@tabler/icons'

import { List, ListStr } from '~/lib/constants'
import notify from '~/lib/notify'
import { trpc } from '~/lib/trpc'

export const Heading = ({ list }: { list: List }) => {
  const utils = trpc.useContext()

  const mutation = trpc.thing.clearTrash.useMutation({
    onSuccess: (data) => {
      notify.success({ message: `Deleted ${data.count} things` })
    },
    onError: (error) => {
      notify.error({ message: error.message })
    },
    onSettled: () => {
      void utils.thing.counts.invalidate()
      void utils.thing.all.invalidate({ list: List.Trash })
    },
  })

  return (
    <Group position='apart'>
      <Title order={1}>{ListStr[list]}</Title>

      {list === List.Trash && (
        <ActionIcon
          size='lg'
          onClick={() => mutation.mutate()}
          title='Delete All Forever'
        >
          <IconTrashX size={24} />
        </ActionIcon>
      )}
    </Group>
  )
}
