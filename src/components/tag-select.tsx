import { MultiSelect, Skeleton } from '@mantine/core'
import cuid from 'cuid'
import NextError from 'next/error'
import { forwardRef } from 'react'

import { trpc } from '~/lib/trpc'

import type { MultiSelectProps } from '@mantine/core'
import type { TagType } from '@prisma/client'

export const TagSelect = forwardRef<HTMLInputElement, { tagType: TagType } & Partial<MultiSelectProps>>(
  function TagSelect(
    { tagType, ...rest }: { tagType: TagType } & Partial<MultiSelectProps>,
    ref,
  ) {
    const utils = trpc.useContext()
    const mutation = trpc.tag.add.useMutation({
      onSuccess: () => {
        void utils.tag.all.invalidate()
      },
    })

    const tagQuery = trpc.tag.all.useQuery({ type: tagType })
    if (tagQuery.error) {
      return (
        <NextError
          title={tagQuery.error.message}
          statusCode={tagQuery.error.data?.httpStatus ?? 500}
        />
      )
    }
    const { data: tags } = tagQuery

    return (
      <MultiSelect
        data-testid='tag select'
        ref={ref}
        data={tagQuery.isSuccess && tags
          ? tags.map((context) => ({
            value: context.id,
            label: context.name,
          }))
          : Array(6).fill('')}
        dropdownComponent={tagQuery.isSuccess ? undefined : Skeleton}
        label={tagType === 'Context' ? 'Context' : 'Waiting For'}
        placeholder={`Type in ${tagType === 'Context' ? 'Contexts' : 'People'}`}
        searchable
        clearable
        creatable
        getCreateLabel={(query) => `+ Create ${query}`}
        onCreate={(query) => {
          const id = cuid()
          const item = { value: id, label: query }
          mutation.mutate({ id: id, type: tagType, name: query })
          return item
        }}
        {...rest}
      />
    )
  },
)
