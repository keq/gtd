import { createStyles } from '@mantine/core'

const useStyles = createStyles((_theme, _params, getRef) => ({
  wrapper: {
    position: 'relative',
    [`&:hover>.${getRef('dragHandle')}`]: {
      opacity: 1,
    },
  },
  dragHandle: {
    opacity: 0,
    ref: getRef('dragHandle'),
    position: 'absolute',
    left: -30,
    top: 15,
    cursor: 'move',
    '&:active': { cursor: 'grabbing' },
    '&:hover': { opacity: 1 },
  },
}))

export default useStyles
