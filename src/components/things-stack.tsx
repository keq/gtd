import { Stack } from '@mantine/core'

import { Row } from '~/components/row'
import type { RowThing } from '~/components/row'
import Sortable from '~/components/sortable'
import SortableItem from '~/components/sortable-item'

export const ThingsStack = ({
  things,
  sortable,
}: {
  things: RowThing[]
  sortable?: boolean
}) => {
  if (!sortable) {
    return (
      <Stack>
        {things.map((thing) => <Row key={thing.id} thing={thing} />)}
      </Stack>
    )
  } else {
    return (
      <Stack>
        <Sortable>
          {things.map((thing) => (
            <SortableItem key={thing.id} id={thing.id}>
              <Row key={thing.id} thing={thing} />
            </SortableItem>
          ))}
        </Sortable>
      </Stack>
    )
  }
}
