import { createStyles } from '@mantine/core'

const useStyles = createStyles((theme) => ({
  header: {
    display: 'flex',
    alignItems: 'center',
    padding: '1em',
    justifyContent: 'space-between',
    height: 'var(--header-height)',
    boxShadow: theme.shadows.xs,
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    zIndex: 10,
  },
}))

export default useStyles
