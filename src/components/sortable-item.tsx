import { useSortable } from '@dnd-kit/sortable'
import { CSS } from '@dnd-kit/utilities'
import { ActionIcon } from '@mantine/core'
import { IconGripVertical } from '@tabler/icons'
import React, { ReactNode } from 'react'

import useStyles from '~/components/sortable-item.styles'

const SortableItem = ({
  id,
  children,
}: {
  id: string
  children: ReactNode
}) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    setActivatorNodeRef,
    transform,
    transition,
  } = useSortable({ id })

  const { classes } = useStyles()

  const style = {
    transform: CSS.Transform.toString(transform && { ...transform, scaleY: 1 }),
    transition,
  }

  return (
    <div
      className={classes.wrapper}
      ref={setNodeRef}
      style={style}
      {...attributes}
    >
      <ActionIcon
        ref={setActivatorNodeRef}
        {...listeners}
        className={classes.dragHandle}
      >
        <IconGripVertical />
      </ActionIcon>
      {children}
    </div>
  )
}
export default SortableItem
