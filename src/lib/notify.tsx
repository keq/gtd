import { NotificationProps, showNotification } from '@mantine/notifications'
import { IconCheck, IconExclamationMark } from '@tabler/icons'

const success = (props: NotificationProps) => {
  showNotification({
    title: 'Success',
    icon: <IconCheck />,
    ...props,
  })
}
const error = (props: NotificationProps) => {
  showNotification({
    title: 'Error',
    color: 'red',
    icon: <IconExclamationMark />,
    ...props,
  })
}

const notify = { success, error }
export default notify
