import { describe, expect, it } from '@jest/globals'
import dayjs, { Dayjs } from 'dayjs'

import { backToFront, dateSchema, diff, DueDateTimeBack, DueDateTimeFront, fmt, frontToBack } from '~/lib/date'

const randDate = () =>
  `${Math.floor(1800 + Math.random() * 1000)}-${(Math.floor(1 + Math.random() * 12)).toString().padStart(2, '0')}-${
    (Math.floor(1 + Math.random() * 28)).toString().padStart(2, '0')
  }`

describe('dateSchema', () => {
  it('correctly validates YYYY-MM-DD', () => {
    expect(dateSchema.safeParse('').success).toBe(false)
    expect(dateSchema.safeParse('2022/08/11').success).toBe(false)
    expect(dateSchema.safeParse('8/11/2022').success).toBe(false)
    expect(dateSchema.safeParse('2022-08-11').success).toBe(true)
  })
  it('correctly validates leap days', () => {
    expect(dateSchema.safeParse('2022-02-29').success).toBe(false)
    expect(dateSchema.safeParse('2024-02-29').success).toBe(true)
  })
})

describe('randDate', () => {
  it('generate valid dates', () => {
    for (let _ = 0; _ < 100; _++) {
      const date = randDate()
      if (!dateSchema.safeParse(date).success) {
        throw new Error(`${date} is invalid`)
      }
    }
  })
})

describe('frontToBack and backToFront', () => {
  it('correctly convert between front-end back-end formats given both date and time', () => {
    // new Date('YYYY-MM-DD') is 00:00:00 UTC
    // new Date('YYYY-MM-DDT00:00:00') is 00:00:00 local time
    for (let _ = 0; _ < 100; _++) {
      const date = randDate()
      const front = {
        dueDate: new Date(`${date}T00:00:00`),
        dueTime: new Date(`${date}T13:00:00`),
      }
      const back = {
        dueDate: date,
        dueTime: new Date(`${date}T13:00:00`),
      }
      expect(frontToBack(front)).toStrictEqual(back)
      expect(backToFront(back)).toStrictEqual(front)

      let x0: DueDateTimeFront = front
      let x1: DueDateTimeBack = back
      for (let _ = 0; _ < 100; _++) {
        x0 = backToFront(x1)
        x1 = frontToBack(x0)
      }
      expect(x0).toStrictEqual(front)
      expect(x1).toStrictEqual(back)
    }
  })
  it('correctly convert between front-end back-end formats given ONLY date', () => {
    // new Date('YYYY-MM-DD') is 00:00:00 UTC
    // new Date('YYYY-MM-DDT00:00:00') is 00:00:00 local time
    for (let _ = 0; _ < 100; _++) {
      const date = randDate()
      const front = {
        dueDate: new Date(`${date}T00:00:00`),
        dueTime: null,
      }
      const back = {
        dueDate: date,
        dueTime: null,
      }
      expect(frontToBack(front)).toStrictEqual(back)
      expect(backToFront(back)).toStrictEqual(front)

      let x0: DueDateTimeFront = front
      let x1: DueDateTimeBack = back
      for (let _ = 0; _ < 100; _++) {
        x0 = backToFront(x1)
        x1 = frontToBack(x0)
      }
      expect(x0).toStrictEqual(front)
      expect(x1).toStrictEqual(back)
    }
  })
})

describe('fmt level 1', () => {
  ;([
    [dayjs(), 'Today'],
    [dayjs().add(1, 'day'), 'Tomorrow'],
    [dayjs().add(-1, 'day'), 'Yesterday'],
    ...[-2, -3, -4, -5, -6, -7, -8, -9].map((n) =>
      [
        dayjs().add(n, 'day'),
        dayjs().add(n, 'day').toDate()
          .toLocaleDateString(undefined, { month: 'short', day: 'numeric' }),
      ] as [Dayjs, string]
    ),
  ] as const).forEach(([in_, out]) => {
    it(`when curr date is ${(new Date()).toString()} formats ${in_.toString()} to ${out}`, () => {
      expect(fmt(1, in_.toDate())).toEqual(out)
    })
  })
  ;([
    ['2023-01-02', [
      ['2023-01-02', 'Today'],
      ['2023-01-03', 'Tomorrow'],
      ['2023-01-04', 'Wed'],
      ['2023-01-05', 'Thu'],
      ['2023-01-06', 'Fri'],
      ['2023-01-07', 'Sat'],
      ['2023-01-08', 'Sun'],
      ['2023-01-09', 'Jan 9'],
    ]],
    ['2023-01-04', [
      ['2023-01-04', 'Today'],
      ['2023-01-05', 'Tomorrow'],
      ['2023-01-06', 'Fri'],
      ['2023-01-07', 'Sat'],
      ['2023-01-08', 'Sun'],
      ['2023-01-09', 'Jan 9'],
      ['2023-01-10', 'Jan 10'],
      ['2023-01-11', 'Jan 11'],
    ]],
    ['2000-12-30', [['2001-01-01', 'Jan 1, 2001']]],
    ['2000-12-31', [['2001-01-01', 'Tomorrow']]],
    ['2001-01-01', [['2000-12-31', 'Yesterday']]],
    ['2000-01-01', [['2000-12-31', 'Dec 31']]],
    ['2001-01-20', [['2000-12-31', 'Dec 31, 2000']]],
    ['2023-02-08', [['2023-02-06', 'Feb 6']]],
  ] as const).forEach(([fakeDate, testCase]) =>
    testCase.forEach(([in_, out]) =>
      it(`when curr date is ${fakeDate} formats ${in_} to ${out}`, () => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date(`${fakeDate}T00:00:00`))
        expect(fmt(1, new Date(`${in_}T00:00:00`))).toEqual(out)
        jest.useRealTimers()
      })
    )
  )
})

describe('fmt level 2', () => {
  ;([
    ['2000-12-30', '2001-01-01', 'Mon, Jan 1, 2001'],
    ['2000-12-30', '2000-01-01', 'Sat, Jan 1'],
    ['2023-01-01', '2023-01-01', 'Sun, Jan 1'],
  ] as const).forEach(([fakeDate, in_, out]) =>
    it(`when curr date is ${fakeDate} formats ${in_} to ${out}`, () => {
      jest.useFakeTimers()
      jest.setSystemTime(new Date(`${fakeDate}T00:00:00`))
      expect(fmt(2, new Date(`${in_}T00:00:00`))).toEqual(out)
      jest.useRealTimers()
    })
  )
})

describe('fmt level 3', () => {
  ;([
    ['2000-12-30', '2001-01-01', 'In 2 Days | Mon, Jan 1, 2001'],
    ['2000-01-28', '2000-01-14', '2 Weeks Ago | Fri, Jan 14'],
    ['2000-01-14', '2000-01-28', 'In 2 Weeks | Fri, Jan 28'],
    ['2000-01-30', '2000-02-02', 'In 3 Days | Wed, Feb 2'],
    ['2000-12-30', '2000-01-01', '11 Months Ago | Sat, Jan 1'],
    ['2023-01-01', '2023-01-01', 'Today | Sun, Jan 1'],
  ] as const).forEach(([fakeDate, in_, out]) =>
    it(`when curr date is ${fakeDate} formats ${in_} to ${out}`, () => {
      jest.useFakeTimers()
      jest.setSystemTime(new Date(`${fakeDate}T00:00:00`))
      expect(fmt(3, new Date(`${in_}T00:00:00`))).toEqual(out)
      jest.useRealTimers()
    })
  )
})

describe('diff', () => {
  ;([
    [new Date('2023-01-01T23:00:00'), new Date('2023-01-03T12:12:12'), [2, 'day']],
    [new Date('2023-01-01'), new Date('2023-01-09'), [1, 'week']],
    [new Date('2023-01-01'), new Date('2023-01-16'), [2, 'week']],
    [new Date('2023-01-01'), new Date('2023-03-16'), [2, 'month']],
    [new Date('2025-01-01'), new Date('2024-12-30'), [-2, 'day']],
    [new Date('2025-01-28'), new Date('2024-01-01'), [-1, 'year']],
    [new Date('1999-01-01'), new Date('1999-01-01'), [0, 'day']],
  ] as const).forEach(([from, to, out]) =>
    it(`returns ${out[0]} ${out[1]} for ${from.toString()} - ${to.toString()}`, () => {
      expect(diff(to, from)).toEqual({ val: out[0], unit: out[1] })
    })
  )
})
