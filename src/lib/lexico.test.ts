import { describe, expect, it } from '@jest/globals'
import shuffle from 'lodash/shuffle'

import Lexico from '~/lib/lexico'

describe('lexico', () => {
  it('generates next position in correct order', () => {
    const poses = []
    let l = new Lexico()
    for (let i = 0; i < 10000; i++) {
      poses.push(l.toString())
      l = l.next()
    }

    expect(poses).toStrictEqual([...poses].sort())
  })
  it('generates prev position in correct order', () => {
    const poses = []
    let l = new Lexico(
      '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~E',
    )
    for (let i = 0; i < 10000; i++) {
      poses.push(l.toString())
      l = l.prev()
    }

    expect(poses).toStrictEqual([...poses].sort().reverse())
  })
  it('calculates betweeners that maintain correct order', () => {
    const poses = ['a', 'b']
    const l = new Lexico(poses[0])
    for (let i = 0; i < 1000; i++) {
      const between = l.between(poses[1])
      poses.splice(1, 0, between.toString())
    }
    expect(poses).toStrictEqual([...poses].sort())
  })
  it('does not run out of previous positions', () => {
    const poses = []
    let l = new Lexico('a')
    for (let i = 0; i < 10000; i++) {
      poses.push(l.toString())
      l = l.prev()
    }

    expect(poses).toStrictEqual([...poses].sort().reverse())
  })
  it('has same sort via its comparison method as javascript lexicographic sort', () => {
    const poses = []
    let l = new Lexico()
    for (let i = 0; i < 10000; i++) {
      poses.push(l.toString())
      l = l.next()
    }

    expect(
      [...shuffle(poses)].sort((a, b) => new Lexico(a).compare(b)),
    ).toStrictEqual([...shuffle(poses)].sort())
  })
})
