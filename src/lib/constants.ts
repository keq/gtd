import {
  IconArchive,
  IconCalendar,
  IconChecklist,
  IconCloud,
  IconInbox,
  IconNotes,
  IconPlayerTrackNext,
  IconTrash,
} from '@tabler/icons'

export const List = {
  Inbox: 'Inbox',
  SomedayMaybe: 'SomedayMaybe',
  Reference: 'Reference',
  Project: 'Project',
  DoIt: 'DoIt',
  NextAction: 'NextAction',
  Calendar: 'Calendar',
  Trash: 'Trash',
  Archive: 'Archive',
} as const

export type List = (typeof List)[keyof typeof List]

export const ListPath = {
  [List.Inbox]: 'inbox',
  [List.SomedayMaybe]: 'someday-maybe',
  [List.Reference]: 'reference',
  [List.Project]: 'project',
  [List.DoIt]: 'do-it',
  [List.NextAction]: 'next-action',
  [List.Calendar]: 'calendar',
  [List.Trash]: 'trash',
  [List.Archive]: 'archive',
} as const

export type ListPath = (typeof ListPath)[keyof typeof ListPath]

export const PathList = {
  inbox: List.Inbox,
  'someday-maybe': List.SomedayMaybe,
  reference: List.Reference,
  project: List.Project,
  'do-it': List.DoIt,
  'next-action': List.NextAction,
  calendar: List.Calendar,
  trash: List.Trash,
  archive: List.Archive,
} as const

export const ListStr = {
  [List.Inbox]: 'Inbox',
  [List.SomedayMaybe]: 'Someday/Maybe',
  [List.Reference]: 'References',
  [List.Project]: 'Projects',
  [List.DoIt]: 'Do It!',
  [List.NextAction]: 'Next Actions',
  [List.Calendar]: 'Calendar',
  [List.Trash]: 'Trash',
  [List.Archive]: 'Archive',
} as const

export const ListIcon = {
  [List.Inbox]: IconInbox,
  [List.SomedayMaybe]: IconCloud,
  [List.Reference]: IconNotes,
  [List.Project]: IconChecklist,
  [List.DoIt]: IconInbox, // TODO replace placeholder
  [List.NextAction]: IconPlayerTrackNext,
  [List.Calendar]: IconCalendar,
  [List.Trash]: IconTrash,
  [List.Archive]: IconArchive,
} as const
