export const doNothing = () => {
  // do nothing
}

export const getDayjsLocale = () => {
  const locale = Intl.DateTimeFormat().resolvedOptions().locale
  import(`dayjs/locale/${locale}.js`).catch(doNothing)
  return locale
}

export const titleCase = (str: string): string => {
  return str
    .split(' ')
    .map((w) => w.length === 0 ? '' : (w[0]?.toLocaleUpperCase() ?? '') + w.substring(1))
    .join(' ')
}

export const throwExpr = (msg: string) => {
  throw new Error(msg)
}
