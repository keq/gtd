import Lexico from '~/lib/lexico'
import type { Context } from '~/server/context'

/**
 * @param ctx - tRPC context containing PrismaClient instance and session info
 * @returns The position for a thing if it were appended: either the default position if there are no other positions, or right after the current last position.
 */
export const appendPos = async (ctx: Context): Promise<string> => {
  const {
    _max: { pos: max_pos },
  } = await ctx.prisma.thing.aggregate({
    _max: { pos: true },
    where: { user: { email: ctx.session?.user?.email ?? undefined } },
  })
  let append_pos: string
  if (max_pos !== null) {
    append_pos = new Lexico(max_pos).next().toString()
  } else {
    append_pos = new Lexico().toString()
  }
  return append_pos
}

/**
 * @param ctx - tRPC context containing PrismaClient instance and session info
 * @returns The position for a thing if it were prepended: either the default position if there are no other positions, or right before the current first position.
 */
export const prependPos = async (ctx: Context): Promise<string> => {
  const {
    _min: { pos: min_pos },
  } = await ctx.prisma.thing.aggregate({
    _min: { pos: true },
    where: { user: { email: ctx.session?.user?.email ?? undefined } },
  })
  let prepend_pos: string
  if (min_pos !== null) {
    prepend_pos = new Lexico(min_pos).prev().toString()
  } else {
    prepend_pos = new Lexico().toString()
  }
  return prepend_pos
}
