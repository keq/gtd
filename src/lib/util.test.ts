import { describe, expect, it } from '@jest/globals'

import { titleCase } from '~/lib/util'

describe('titleCase', () => {
  it('correctly titlecases', () => {
    const cases = [
      ['', ''],
      [' ', ' '],
      ['  ', '  '],
      ['ØSTER HUS AS', 'ØSTER HUS AS'],
      ['øster hus as', 'Øster Hus As'],
      ['Jo-Ann Smith', 'Jo-Ann Smith'],
      ['today 9:51 AM', 'Today 9:51 AM'],
    ] as const
    for (const [in_, out] of cases) {
      expect(titleCase(in_)).toEqual(out)
    }
  })
})
