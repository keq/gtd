import dayjs from 'dayjs'
import isoWeek from 'dayjs/plugin/isoWeek'
import { z } from 'zod'

import { titleCase } from '~/lib/util'

import type { Dayjs } from 'dayjs'

dayjs.extend(isoWeek)

const dateRegex = /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/

export const dateSchema = z
  .string()
  .regex(dateRegex)
  .refine((str) => dayjs(str).format('YYYY-MM-DD') === str)

export interface DueDateTimeFront {
  dueDate: Date
  dueTime: Date | null
}
export interface DueDateTimeBack {
  dueDate: string
  dueTime: Date | null
}

// Mantine's date input returns date with zeroed out time in LOCAL tz
// this means when converted to UTC the date may change
export const frontToBack = ({
  dueDate: dueDateIn,
  dueTime: dueTimeIn,
}: DueDateTimeFront): DueDateTimeBack => {
  const dueDate = dayjs(dueDateIn)
  // make sure dueTime is always a full datetime synced to dueDate
  const dueTime = dueTimeIn === null
    ? null
    : dayjs(dueTimeIn)
      .year(dueDate.year())
      .month(dueDate.month())
      .date(dueDate.date())
      .toDate()

  return { dueDate: dateSchema.parse(dueDate.format('YYYY-MM-DD')), dueTime }
}

export const backToFront = ({
  dueDate: dueDateIn,
  dueTime: dueTimeIn,
}: DueDateTimeBack): DueDateTimeFront => {
  return {
    dueDate: dueTimeIn === null
      ? new Date(`${dueDateIn}T00:00:00`)
      : dayjs(dueTimeIn).hour(0).minute(0).second(0).toDate(),
    dueTime: dueTimeIn,
  }
}

type Formatter = (date: Date, time?: Date | null) => string

const zero = (date: Dayjs): Dayjs => {
  return date.hour(0).minute(0).second(0).millisecond(0)
}

type DateUnit = 'day' | 'week' | 'month' | 'year'

export const diff = (to: Date, from?: Date): { val: number; unit: DateUnit } => {
  const helper = (unit: DateUnit): number => {
    const diff = zero(dayjs(to)).diff(zero(dayjs(from)), unit, true)
    // days aren't always 24 hours
    return unit === 'day' ? Math.round(diff) : diff | 0
  }
  const days = helper('day')
  const weeks = helper('week')
  const months = helper('month')
  const years = helper('year')

  if (Math.abs(years) >= 1) {
    return { val: years, unit: 'year' }
  } else if (Math.abs(months) >= 1) {
    return { val: months, unit: 'month' }
  } else if (Math.abs(weeks) >= 1) {
    return { val: weeks, unit: 'week' }
  } else {
    return { val: days, unit: 'day' }
  }
}
const fmtRel = (date: Date): string => {
  const { val, unit } = diff(date)
  const rtf = new Intl.RelativeTimeFormat(undefined, { numeric: 'auto', style: 'long' })
  return titleCase(rtf.format(val, unit))
}

const isThisWeek = (date: Date): boolean =>
  isThisYear(date) && dayjs(date).month() === dayjs().month() && dayjs(date).isoWeek() === dayjs().isoWeek()
const isThisYear = (date: Date): boolean => dayjs(date).year() === dayjs().year()

const fmt1: Formatter = (date, time) => {
  const { val, unit } = diff(date)

  let fmted
  if (unit === 'day' && Math.abs(val) <= 1) {
    fmted = fmtRel(date)
  } else if (val > 0 && isThisWeek(date)) {
    fmted = date.toLocaleDateString(undefined, { weekday: 'short' })
  } else {
    fmted = date.toLocaleDateString(undefined, {
      year: isThisYear(date) ? undefined : 'numeric',
      month: 'short',
      day: 'numeric',
    })
  }

  if (time == null) {
    return fmted
  } else {
    return `${fmted} ${time.toLocaleTimeString(undefined, { hour: 'numeric', minute: 'numeric' })}`
  }
}

const fmt2: Formatter = (date, time) => {
  const opts: Intl.DateTimeFormatOptions = {
    year: isThisYear(date) ? undefined : 'numeric',
    month: 'short',
    day: 'numeric',
    weekday: 'short',
  }
  if (time == null) {
    return date.toLocaleDateString(undefined, opts)
  } else {
    return time.toLocaleString(undefined, {
      ...opts,
      hour: 'numeric',
      minute: 'numeric',
    })
  }
}
const fmt3: Formatter = (date, time) => {
  return `${fmtRel(date)} | ${fmt2(date, time)}`
}

export const fmt = (level: 1 | 2 | 3, date: Date, time?: Date | null) => {
  if (level === 1) {
    return fmt1(date, time)
  } else if (level === 2) {
    return fmt2(date, time)
  } else {
    return fmt3(date, time)
  }
}
