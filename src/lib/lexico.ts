const START_CHAR_CODE = 32
const END_CHAR_CODE = 126
const FIRST_POSITION = String.fromCharCode(START_CHAR_CODE + 1)

export default class Lexico {
  pos: string

  constructor(pos: string = FIRST_POSITION) {
    this.pos = pos
    assertDev(this.isValid())
  }

  toString(): string {
    return this.pos
  }

  isValid(): boolean {
    if (
      this.pos === ''
      || this.pos.charCodeAt(this.pos.length - 1) === START_CHAR_CODE
    ) {
      return false
    }
    for (let i = 0; i < this.pos.length; i++) {
      const t = this.pos.charCodeAt(i)
      if (t < START_CHAR_CODE || t > END_CHAR_CODE) {
        return false
      }
    }
    return true
  }

  compare(pos: string): number {
    return +(pos < this.pos) - +(pos > this.pos)
  }

  prev(): Lexico {
    for (let i = this.pos.length - 1; i >= 0; i--) {
      const curCharCode = this.pos.charCodeAt(i)
      if (curCharCode > START_CHAR_CODE + 1) {
        const position = this.pos.slice(0, i) + String.fromCharCode(curCharCode - 1)
        assertDev(this.isValid())
        return new Lexico(position)
      }
    }
    const position = this.pos.slice(0, this.pos.length - 1)
      + String.fromCharCode(START_CHAR_CODE)
      + String.fromCharCode(END_CHAR_CODE)
    return new Lexico(position)
  }
  next(): Lexico {
    for (let i = this.pos.length - 1; i >= 0; i--) {
      const curCharCode = this.pos.charCodeAt(i)
      if (curCharCode < END_CHAR_CODE) {
        const position = this.pos.slice(0, i) + String.fromCharCode(curCharCode + 1)
        return new Lexico(position)
      }
    }
    const position = this.pos + String.fromCharCode(START_CHAR_CODE + 1)
    return new Lexico(position)
  }
  between(pos: string): Lexico {
    assertDev(this.pos < pos)
    let flag = false
    let position = ''
    const maxLength = Math.max(this.pos.length, pos.length)
    for (let i = 0; i < maxLength; i++) {
      const lower = i < this.pos.length ? this.pos.charCodeAt(i) : START_CHAR_CODE
      const upper = i < pos.length && !flag ? pos.charCodeAt(i) : END_CHAR_CODE
      if (lower === upper) {
        position += String.fromCharCode(lower)
      } else if (upper - lower > 1) {
        position += String.fromCharCode(avg(lower, upper))
        flag = false
        break
      } else {
        position += String.fromCharCode(lower)
        flag = true
      }
    }

    if (flag) {
      position += String.fromCharCode(avg(START_CHAR_CODE, END_CHAR_CODE))
    }
    assertDev(this.pos < position)
    assertDev(position < pos)
    return new Lexico(position)
  }
}

const assertDev = (expr: boolean) => {
  if (!expr) {
    throw Error('Assertion Error')
  }
}

const avg = (a: number, b: number): number => {
  return Math.trunc((a + b) / 2)
}
