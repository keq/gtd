import { PrismaAdapter } from '@next-auth/prisma-adapter'
import NextAuth, { NextAuthOptions } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'
import GitHubProvider from 'next-auth/providers/github'
import GitlabProvider from 'next-auth/providers/gitlab'

import { seedDemoUser } from 'prisma/seed'
import { prisma } from '~/lib/prisma'
import { env } from '~/server/env'

export const authOptions: NextAuthOptions = {
  debug: process.env.NODE_ENV !== 'production',
  theme: {
    logo: '/icon.svg',
  },
  providers: [
    GitHubProvider({
      clientId: env.GITHUB_ID,
      clientSecret: env.GITHUB_SECRET,
    }),
    GitlabProvider({
      clientId: env.GITLAB_ID,
      clientSecret: env.GITLAB_SECRET,
    }),
    CredentialsProvider({
      id: 'credentials',
      name: 'Demo Account',
      credentials: {},
      async authorize() {
        return await seedDemoUser()
      },
    }),
  ],
  adapter: PrismaAdapter(prisma),
  session: { strategy: 'jwt' },
}

export default NextAuth(authOptions)
