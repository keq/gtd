import dayjs from 'dayjs'
import { createEvents } from 'ics'

import { prisma } from '~/lib/prisma'

import type { NextApiRequest, NextApiResponse } from 'next'

const handle = async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { webcalId },
    method,
  } = req

  if (webcalId === undefined || Array.isArray(webcalId) || method === undefined) {
    res.status(400).end()
    return
  }

  switch (method) {
    case 'GET': {
      // check user before since invalid webcalId to calendar query will return empty array rather than null
      const user = await prisma.user.findUnique({
        where: { webcalId: webcalId },
      })

      if (user === null) {
        res.status(404).end()
        return
      }

      const things = await prisma.thing.findMany({
        where: { user: { id: user.id } },
        select: { name: true, dueDate: true, dueTime: true },
      })

      const events = things.map((thing) => {
        if (thing.dueTime === null) {
          return {
            title: thing.name,
            start: thing.dueDate?.split('-').map((x) => Number(x)) as [
              number,
              number,
              number,
            ],
            end: dayjs(thing.dueDate)
              .add(1, 'day')
              .format('YYYY-M-D')
              .split('-')
              .map((x) => Number(x)),
          }
        } else {
          return {
            title: thing.name,
            start: dayjs(thing.dueTime)
              .format('YYYY-M-D-H-m')
              .split('-')
              .map((x) => Number(x)) as [
                number,
                number,
                number,
                number,
                number,
              ],
            duration: { minutes: 30 },
          }
        }
      })

      // TODO: fix types upstream
      // https://stackoverflow.com/questions/37688318/typescript-interface-possible-to-make-one-or-the-other-properties-required
      //
      /* eslint-disable @typescript-eslint/no-explicit-any */
      /* eslint-disable @typescript-eslint/no-unsafe-argument */
      const { error, value } = createEvents(events as any)
      if (error) {
        console.error(error)
        res.status(500).end()
        return
      }

      res.setHeader(
        'Content-Disposition',
        'attachment; filename="calendar.ics"',
      )
      res.setHeader('Content-Type', 'text/calendar')
      res.status(200).send(value)
      break
    }
    default: {
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
    }
  }
}

export default handle
