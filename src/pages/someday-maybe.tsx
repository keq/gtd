
import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const SomedayMaybesPage: NextPage = () => {
  return <ListStack input={{list: List.SomedayMaybe}} />
}

export default SomedayMaybesPage
