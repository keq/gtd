import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const ProjectsPage: NextPage = () => {
  return <ListStack input={{ list: List.Project }} />
}

export default ProjectsPage
