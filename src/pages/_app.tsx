import NextError from 'next/error'
import Script from 'next/script'
import React, { ReactNode } from 'react'
import 'styles/globals.css'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import dynamic from 'next/dynamic'
import Head from 'next/head'

import { ListPath, ListStr, PathList } from '~/lib/constants'
import { trpc } from '~/lib/trpc'
import { getDayjsLocale } from '~/lib/util'

import type { ColorScheme } from '@mantine/core'
import type { Session } from 'next-auth'
import type { AppProps } from 'next/app'

const SessionProvider = dynamic(() => import('next-auth/react').then((mod) => mod.SessionProvider))
const ColorSchemeProvider = dynamic(() => import('@mantine/core').then((mod) => mod.ColorSchemeProvider))
const MantineProvider = dynamic(() => import('@mantine/core').then((mod) => mod.MantineProvider))
const NotificationsProvider = dynamic(() => import('@mantine/notifications').then((mod) => mod.NotificationsProvider))
const AppShell = dynamic(() => import('~/components/app-shell').then((mod) => mod.AppShell))

const listPaths = Object.values(ListPath)
const isListPath = (path: string): path is ListPath => {
  return (listPaths as string[]).includes(path)
}

const App = ({
  Component,
  pageProps,
  ...appProps
}: AppProps<{ session: Session }>) => {
  const path = appProps.router.pathname.replace('/', '')
  const title = isListPath(path)
    ? `${ListStr[PathList[path]]} - Next of Thing`
    : 'Next of Thing'

  const withLayout = (child: ReactNode) => {
    if (isListPath(path)) {
      return <AppShell path={path}>{child}</AppShell>
    } else {
      return child
    }
  }

  const darkThemeQuery = trpc.user.darkTheme.useQuery()

  const utils = trpc.useContext()
  const mutation = trpc.user.toggleDarkTheme.useMutation({
    onMutate: async () => {
      await utils.user.darkTheme.cancel()
      return { prev: utils.user.darkTheme.getData() }
    },
    onError: (_err, _in, ctx) => {
      utils.user.darkTheme.setData(undefined, ctx?.prev)
    },
    onSettled: () => {
      void utils.user.darkTheme.invalidate()
    },
  })

  let colorScheme: ColorScheme
  if (isListPath(path)) {
    if (darkThemeQuery.error) {
      return (
        <NextError
          title={darkThemeQuery.error.message}
          statusCode={darkThemeQuery.error.data?.httpStatus ?? 500}
        />
      )
    }

    if (darkThemeQuery.status !== 'success') {
      return null
    }
    const { data: darkTheme } = darkThemeQuery
    colorScheme = darkTheme ? 'dark' : 'light'
  } else {
    colorScheme = 'light'
  }

  return (
    <SessionProvider session={pageProps.session}>
      <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={() => mutation.mutate()}>
        <MantineProvider
          withGlobalStyles
          withNormalizeCSS
          withCSSVariables
          theme={{
            colorScheme,
            datesLocale: getDayjsLocale(),
          }}
        >
          <NotificationsProvider>
            <Head>
              <title>{title}</title>
            </Head>
            <Script
              data-domain='nextofthing.com'
              src='/pls/pls.js'
              data-api='/pls/event'
            />
            {withLayout(<Component {...pageProps} />)}
          </NotificationsProvider>
          {process.env.NODE_ENV !== 'production' && <ReactQueryDevtools />}
        </MantineProvider>
      </ColorSchemeProvider>
    </SessionProvider>
  )
}

export default trpc.withTRPC(App)
