import Error from 'next/error'

export const getStaticProps = () => ({ props: {} })
export default function Page() {
  return <Error statusCode={404} />
}
