import { Group, Paper, Stack } from '@mantine/core'
import { useForm } from '@mantine/form'

import { ListStack } from '~/components/list-stack'
import { TagSelect } from '~/components/tag-select'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const NextActionsPage: NextPage = () => {
  const form = useForm({
    initialValues: {
      contextIds: [],
      waitingForIds: [],
    },
  })

  const tagIds = [...form.values.contextIds, ...form.values.waitingForIds]

  return (
    <Stack>
      <Paper
        pos='sticky'
        top={0}
        p='md'
        shadow='sm'
        sx={{ zIndex: 1 }}
      >
        <Group grow>
          <TagSelect tagType='Context' {...form.getInputProps('contextIds')} />
          <TagSelect tagType='WaitingFor' {...form.getInputProps('waitingForIds')} />
        </Group>
      </Paper>
      <ListStack input={{ list: List.NextAction, tagIds }} />
    </Stack>
  )
}

export default NextActionsPage
