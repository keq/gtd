import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const TrashPage: NextPage = () => {
  return <ListStack input={{ list: List.Trash }} />
}

export default TrashPage
