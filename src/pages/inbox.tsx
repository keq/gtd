import { Stack } from '@mantine/core'

import { ListStack } from '~/components/list-stack'
import { Add } from '~/components/row'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const InboxPage: NextPage = () => {
  return (
    <Stack>
      <Add list={List.Inbox} />
      <ListStack input={{ list: List.Inbox }} />
    </Stack>
  )
}

export default InboxPage
