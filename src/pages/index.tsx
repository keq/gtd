import { NextPage } from 'next'

import { Landing } from '~/components/landing'

export const getStaticProps = () => ({ props: {} })
const Home: NextPage = () => {
  return <Landing />
}

export default Home
