import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const ArchivePage: NextPage = () => {
  return <ListStack input={{ list: List.Archive }} />
}

export default ArchivePage
