import { Anchor, Button, Group, Indicator, Paper, Skeleton, Stack, Title } from '@mantine/core'
import { DatePicker } from '@mantine/dates'
import { IconArrowLeft, IconArrowRight } from '@tabler/icons'
import dayjs from 'dayjs'
import NextError from 'next/error'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

import { Row } from '~/components/row'
import WebcalLink from '~/components/webcal-link'
import { List } from '~/lib/constants'
import { fmt } from '~/lib/date'
import { RouterOutput, trpc } from '~/lib/trpc'

import type { NextPage } from 'next'

// height of sticky
const SCROLL_MARGIN = 68

const FORMAT = 'YYYY-MM-DD'
const OVERDUE = 'Overdue'

const deserialize = (date: string) => dayjs(date, FORMAT).toDate()
const serialize = (date: Date) => dayjs(date).format(FORMAT)

const organize = (things: RouterOutput['thing']['all']) => {
  const map = new Map<string | null, typeof things>()
  for (const thing of things) {
    if (thing.dueDate === null) throw new Error('null date found in Calendar')
    const date = thing.dueDate
    const key = dayjs().isAfter(date, 'day')
      ? null
      : serialize(date)
    const preexist = map.get(key)
    map.set(key, preexist !== undefined ? preexist.concat(thing) : [thing])
  }
  return map
}

const order = (organized: ReturnType<typeof organize>) => {
  const keyToIdx = new Map<string | null, number>()
  const ordered: (string | null)[] = []
  let i = 0
  for (const [key, _] of organized) {
    ordered.push(key)
    keyToIdx.set(key, i)
    i++
  }
  const len = i

  const relIndex = (day: Date | null, i: number): string | null => {
    const idx = keyToIdx.get(day === null ? null : serialize(day))
    if (idx === undefined) return null
    const res = ordered.at((idx + i) % len)
    return res ?? null
  }
  const firstNonNull = ordered.at(1) ?? null
  const lastNonNull = ordered.at(-1) ?? null

  return { firstNonNull, lastNonNull, relIndex }
}

const GoToDateButton = (
  { date, dir, onClick }: { date: string | null; dir: 'left' | 'right'; onClick: () => void },
) => {
  return (
    <Button
      styles={(theme) => ({
        root: {
          flex: '1 1 auto',
          [theme.fn.smallerThan('sm')]: { padding: theme.spacing.md },
        },
        label: {
          [theme.fn.smallerThan('sm')]: { display: 'none' },
        },
        icon: {
          [theme.fn.smallerThan('sm')]: { margin: 0 },
        },
      })}
      onClick={onClick}
      disabled={date === null}
      leftIcon={dir === 'left' ? <IconArrowLeft /> : undefined}
      rightIcon={dir === 'right' ? <IconArrowRight /> : undefined}
      variant='light'
      title={date === null ? undefined : fmt(3, deserialize(date))}
    >
      {date === null
        ? undefined
        // use format similar to datepicker's
        : (deserialize(date)).toLocaleDateString(undefined, { year: 'numeric', month: 'short', day: 'numeric' })}
    </Button>
  )
}

const CalendarPage: NextPage = () => {
  const [day, setDay] = useState<Date | null>(null)
  const router = useRouter()
  const goTo = (headingId: string) => {
    void router.replace(`#${headingId}`, undefined, { shallow: true })
  }
  useEffect(() => {
    if (day !== null) {
      goTo(serialize(day))
    }
  }, [day]) // eslint-disable-line react-hooks/exhaustive-deps
  const thingsQuery = trpc.thing.all.useQuery({ list: List.Calendar })

  if (thingsQuery.error) {
    return (
      <NextError
        title={thingsQuery.error.message}
        statusCode={thingsQuery.error.data?.httpStatus ?? 500}
      />
    )
  }

  if (thingsQuery.status !== 'success') {
    return (
      <Stack>
        {Array.from(Array(10).keys()).map((i) => <Skeleton key={i} height={60} />)}
      </Stack>
    )
  }
  const { data: things } = thingsQuery
  const organized = organize(things)
  const { firstNonNull, lastNonNull, relIndex } = order(organized)
  const before = relIndex(day, -1) ?? lastNonNull
  const after = relIndex(day, 1) ?? firstNonNull

  return (
    <Stack>
      <WebcalLink />
      <Paper p='md' shadow='xs' pos='sticky' top={0} style={{ zIndex: 1 }}>
        <Group position='apart' noWrap>
          <GoToDateButton
            date={before}
            dir='left'
            onClick={() => {
              if (before === null) return
              setDay(deserialize(before))
            }}
          />
          <DatePicker
            style={{ flex: '2 1 auto' }}
            dropdownPosition={'bottom' as 'flip'}
            placeholder='Jump to Date'
            fullWidth
            minDate={new Date()}
            value={day}
            onChange={setDay}
            excludeDate={(date) => {
              return organized.get(serialize(date)) === undefined
            }}
            renderDay={(date) => {
              const day = date.getDate()
              const val = organized.get(serialize(date))
              const count = val === undefined ? '' : val.length.toString()
              return (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <Indicator
                    size={18}
                    label={count}
                    disabled={count === ''}
                    color='red'
                    offset={-8}
                    style={{ lineHeight: 1 }}
                  >
                    {day.toString()}
                  </Indicator>
                </div>
              )
            }}
          />
          <GoToDateButton
            date={after}
            dir='right'
            onClick={() => {
              if (after === null) return
              setDay(deserialize(after))
            }}
          />
        </Group>
      </Paper>
      {Array.from(organized).map(([dateStr, things]) => {
        const link = dateStr ?? OVERDUE
        const label = dateStr === null
          ? OVERDUE
          : fmt(2, deserialize(dateStr))
        return (
          <>
            <Title order={3} id={link} style={{ scrollMargin: SCROLL_MARGIN }}>
              <Anchor href={`#${link}`} variant='text'>{label}</Anchor>
            </Title>
            {things.map((thing) => <Row key={thing.id} thing={thing} />)}
          </>
        )
      })}
    </Stack>
  )
}

export default CalendarPage
