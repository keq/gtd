import { ListStack } from '~/components/list-stack'
import { List } from '~/lib/constants'

import type { NextPage } from 'next'

const ReferencesPage: NextPage = () => {
  return <ListStack input={{ list: List.Reference }} />
}

export default ReferencesPage
