#!/bin/sh

COMMIT="4df3f9262d84cab0039c07bf861045fbb3c20ab7"
HOST="db:5432"

wget -qO - "https://raw.githubusercontent.com/eficode/wait-for/$COMMIT/wait-for" | sh -s -- "$HOST" -- npx prisma migrate deploy
