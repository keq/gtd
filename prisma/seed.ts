import { PrismaClient, TagType } from '@prisma/client'
import cuid from 'cuid'
import dayjs from 'dayjs'

import { defaultUserSelect } from '~/server/defaultSelectors'

// ~/ doesn't work
import { List } from '../src/lib/constants'
import Lexico from '../src/lib/lexico'

const noteSample =
  '<h2>list status of all git repos</h2><pre><code class="language-bash"> find ~ -name ".git" 2&gt; /dev/null | sed \'s/\\/.git/\\//g\' | awk \'{print "-------------------------\\n\\033[1;32mGit Repo:\\033[0m " $1; system("git --git-dir="$1".git --work-tree="$1" status")}\'</code></pre><h2>Convert directory of videos to MP4 in parallel</h2><pre><code class="language-bash">for INPUT in *.avi ; do echo "${INPUT%.avi}" ; done | xargs -i -P9  HandBrakeCLI -i "{}".avi -o "{}".mp4</code></pre>'

const longPassage =
  'Merzbow, Boredoms, Gerogerigegege, Coil, Throbbing Gristle, Whitehouse, Nurse with Wound, Einstürzende Neubauten, Brainbombs, Egor Letov, Death in June, Current 93, La Monte Young, Moondog, Lou Harrison, Henry Cowell, Luigi Russolo, Popol Vuh, Fishmans, Jean Jacques Perrey, Les Rallizes Dénudés, Rainbow Caroliner, Taj Mahal Travellers, Fushitsusha, Peter Brötzmann, John Cage, Scott Walker, Unwound, Dead, Frank Zappa, Morton Feldman, Captain Beefheart, Pharoah Sanders, Albert Ayler, Ornette Coleman, Alice Coltrane, Arnold Schoenberg, Pierre Boulez, György Ligeti, Karlheinz Stockhausen, Nang Nang, Thinking Fellers Union Local 282, Nara Leão, Basic Channel, Raymond Scott, Delia Derbyshire, Daphne Oram, Noah Howard, Terry Riley, Peter Sotos, Lula Côrtes e Zé Ramalho, Boyd Rice, Mahmoud Ahmed, Henry Flynt, Kazumoto Endo, David Tudor, Aporea, Half Japanese, Mega Banton, Secret Chiefs 3, Keiji Haino, Ramleh, Otomo Yoshihide, John Zorn, Joe Meek, Robbie Basho, Phil Spector, Faxed Head, Harry Partch, Wesl'

const longWord =
  'https://www.google.com/search?hl=en&q=Maloof%2C%20A.%20C.%3B%20Porter%2C%20S.%20M.%3B%20Moore%2C%20J.%20L.%3B%20Dudas%2C%20F.%20O.%3B%20Bowring%2C%20S.%20A.%3B%20Higgins%2C%20J.%20A.%3B%20Fike%2C%20D.%20A.%3B%20Eddy%2C%20M.%20P.%20(2010).%20%22The%20earliest%20Cambrian%20record%20of%20animals%20and%20ocean%20geochemical%20change%22.%20Geological%20Society%20of%20America%20Bulletin.%20122%20(11%2D12)%3A%201731%2D1774.%20Bibcode%3A2010GSAB..122.1731M.%20doi%3A10.1130%2FB30346.1.'

export const Seed = {
  Inbox: { A: 'take out trash', B: longPassage, C: longWord },
  Trash: {
    A: 'advent of code 2021',
    B: 'garlic',
    C: 'a',
    D: 'test',
    NA: 'get package from mailbox',
    PA: 'clean room',
    PA_A: 'vacuum floor',
  },
  SomedayMaybe: { A: 'start juggling equipment business', B: 'Anna Karenina' },
  Reference: { A: 'helpful oneliners', B: 'https://en.wikipedia.org/wiki/Milliken_v._Bradley' },
  Note: { A: noteSample },
  Project: {
    A: 'plan vacation',
    B: 'organize home office',
  },
  NextAction: {
    PA: {
      A: 'ask friends for recommendations',
      B: 'make itinerary with family',
      C: 'make hotel reservations',
    },
    PB: {
      A: 'take measurements for shelving and furniture',
      B: 'clear out clutter',
    },
    A: 'cancel subscription',
    TA: 'mop kitchen floor',
    TB: "ask coworker if they've seen pen",
    TC: 'ask vendor for updated lead time',
    TD: 'get application reviewed',
  },
  WaitingFor: { A: 'Bob', B: 'Alice' },
  Context: { A: 'home', B: 'work', C: 'phone', D: 'park' },
  Date: {
    A: dayjs().subtract(3, 'month').hour(7).minute(0),
    B: dayjs().subtract(1, 'month').hour(13).minute(35),
    C: dayjs().subtract(1, 'week').hour(13).minute(0),
    D: dayjs().subtract(3, 'day').hour(12).minute(0),
    E: dayjs().subtract(2, 'day').hour(11).minute(15),
    F: dayjs().subtract(1, 'day').hour(9).minute(30),
    G: dayjs().hour(23).minute(59),
    H: dayjs().add(1, 'day').hour(5).minute(0),
    I: dayjs().add(2, 'day').hour(7).minute(0),
    J: dayjs().add(3, 'day').hour(16).minute(45),
    K: dayjs().add(1, 'week').hour(17).minute(0),
    L: dayjs().add(1, 'month').hour(21).minute(30),
    M: dayjs().add(3, 'month').hour(9).minute(0),
  },
  Calendar: {
    A: 'submit application',
    B: 'Baker Dental appointment',
    C: 'submit progress report',
    D: 'Hills Dentistry appointment',
    E: 'send revision',
    F: 'optometrist appointment',
    G: 'pick up award',
    H: 'lunch date',
    I: 'return award',
    J: 'give friend a ride to mikes',
    K: 'check PO box for important package',
    L: 'car repair',
    M: 'check on boat',
    N: 'turn in paper',
    O: 'check prices',
    P: 'orientation',
    Q: 'return borrowed equipment',
    R: 'dinner with person',
    S: 'prepare guest room',
    T: 'tune into broadcast',
    U: 'shred credit card',
    V: 'snipe auction',
    W: 'check on neighbor',
    X: 'brainstorming meeting',
    Y: 'wait for Godot',
    Z: 'Eastern Dental appointment',
  },
  Archive: {
    PA_A: 'research flight paths and prices',
    P_A: 'write letter of recommendation',
    P_A_A: 'look at LOR templates',
    DI: 'bring back bins',
    NA: 'do dishes',
  },
} as const

// names must be unique
const MetaData = {
  CalendarDate: {
    [Seed.Calendar.A]: { 'dueDate': Seed.Date.A.format('YYYY-MM-DD'), 'dueTime': Seed.Date.A.toDate() },
    [Seed.Calendar.B]: { 'dueDate': Seed.Date.A.format('YYYY-MM-DD') },
    [Seed.Calendar.C]: { 'dueDate': Seed.Date.B.format('YYYY-MM-DD'), 'dueTime': Seed.Date.B.toDate() },
    [Seed.Calendar.D]: { 'dueDate': Seed.Date.B.format('YYYY-MM-DD') },
    [Seed.Calendar.E]: { 'dueDate': Seed.Date.C.format('YYYY-MM-DD'), 'dueTime': Seed.Date.C.toDate() },
    [Seed.Calendar.F]: { 'dueDate': Seed.Date.C.format('YYYY-MM-DD') },
    [Seed.Calendar.G]: { 'dueDate': Seed.Date.D.format('YYYY-MM-DD'), 'dueTime': Seed.Date.D.toDate() },
    [Seed.Calendar.H]: { 'dueDate': Seed.Date.D.format('YYYY-MM-DD') },
    [Seed.Calendar.I]: { 'dueDate': Seed.Date.E.format('YYYY-MM-DD'), 'dueTime': Seed.Date.E.toDate() },
    [Seed.Calendar.J]: { 'dueDate': Seed.Date.E.format('YYYY-MM-DD') },
    [Seed.Calendar.K]: { 'dueDate': Seed.Date.F.format('YYYY-MM-DD'), 'dueTime': Seed.Date.F.toDate() },
    [Seed.Calendar.L]: { 'dueDate': Seed.Date.F.format('YYYY-MM-DD') },
    [Seed.Calendar.M]: { 'dueDate': Seed.Date.G.format('YYYY-MM-DD'), 'dueTime': Seed.Date.G.toDate() },
    [Seed.Calendar.N]: { 'dueDate': Seed.Date.G.format('YYYY-MM-DD') },
    [Seed.Calendar.O]: { 'dueDate': Seed.Date.H.format('YYYY-MM-DD'), 'dueTime': Seed.Date.H.toDate() },
    [Seed.Calendar.P]: { 'dueDate': Seed.Date.H.format('YYYY-MM-DD') },
    [Seed.Calendar.Q]: { 'dueDate': Seed.Date.I.format('YYYY-MM-DD'), 'dueTime': Seed.Date.I.toDate() },
    [Seed.Calendar.R]: { 'dueDate': Seed.Date.I.format('YYYY-MM-DD') },
    [Seed.Calendar.S]: { 'dueDate': Seed.Date.J.format('YYYY-MM-DD'), 'dueTime': Seed.Date.J.toDate() },
    [Seed.Calendar.T]: { 'dueDate': Seed.Date.J.format('YYYY-MM-DD') },
    [Seed.Calendar.U]: { 'dueDate': Seed.Date.K.format('YYYY-MM-DD'), 'dueTime': Seed.Date.K.toDate() },
    [Seed.Calendar.V]: { 'dueDate': Seed.Date.K.format('YYYY-MM-DD') },
    [Seed.Calendar.W]: { 'dueDate': Seed.Date.L.format('YYYY-MM-DD'), 'dueTime': Seed.Date.L.toDate() },
    [Seed.Calendar.X]: { 'dueDate': Seed.Date.L.format('YYYY-MM-DD') },
    [Seed.Calendar.Y]: { 'dueDate': Seed.Date.M.format('YYYY-MM-DD'), 'dueTime': Seed.Date.M.toDate() },
    [Seed.Calendar.Z]: { 'dueDate': Seed.Date.M.format('YYYY-MM-DD') },
  },
} as const

const prisma = new PrismaClient()

let curr = new Lexico(' !')
const pos = () => {
  curr = curr.next()
  return curr.toString()
}

const seedUser = async (user: { id: string; name: string | null; email: string | null; temporary: boolean }) => {
  const prev = await prisma.user.findUnique({ where: { id: user.id } })
  if (prev === null) {
    await prisma.user.create({
      data: {
        id: user.id,
        name: user.name,
        email: user.email,
        temporary: user.temporary,
      },
    })
  } else {
    await prisma.$transaction([
      prisma.thing.deleteMany({ where: { userId: user.id } }),
      prisma.tag.deleteMany({ where: { userId: user.id } }),
    ])
  }
  return await prisma.user.update({
    where: {
      id: user.id,
    },
    data: {
      tags: {
        create: [
          { type: TagType.Context, name: Seed.Context.A },
          { type: TagType.Context, name: Seed.Context.B },
          { type: TagType.Context, name: Seed.Context.C },
          { type: TagType.Context, name: Seed.Context.D },
          { type: TagType.WaitingFor, name: Seed.WaitingFor.A },
          { type: TagType.WaitingFor, name: Seed.WaitingFor.B },
        ],
      },
      things: {
        create: [
          { list: List.Inbox, name: Seed.Inbox.A, pos: pos() },
          { list: List.Inbox, name: Seed.Inbox.B, pos: pos() },
          { list: List.Inbox, name: Seed.Inbox.C, pos: pos() },
          {
            list: List.Inbox,
            name: Seed.Trash.A,
            pos: pos(),
            trashed: new Date(),
          },
          {
            list: List.Inbox,
            name: Seed.Trash.B,
            pos: pos(),
            trashed: new Date(),
          },
          {
            list: List.Inbox,
            name: Seed.Trash.C,
            pos: pos(),
            trashed: new Date(),
          },
          {
            list: List.Inbox,
            name: Seed.Trash.D,
            pos: pos(),
            trashed: new Date(),
          },
          {
            list: List.NextAction,
            name: Seed.Trash.NA,
            pos: pos(),
            trashed: new Date(),
            tags: {
              create: [{
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.Context.D },
                  },
                },
              }],
            },
          },
          {
            list: List.SomedayMaybe,
            name: Seed.SomedayMaybe.A,
            pos: pos(),
          },
          {
            list: List.SomedayMaybe,
            name: Seed.SomedayMaybe.B,
            pos: pos(),
          },
          {
            list: List.Reference,
            name: Seed.Reference.A,
            pos: pos(),
            note: Seed.Note.A,
          },
          {
            list: List.Reference,
            name: Seed.Reference.B,
            pos: pos(),
          },
          {
            list: List.Project,
            name: Seed.Trash.PA,
            pos: pos(),
            trashed: dayjs().subtract(3, 'day').toDate(),
            nextActions: {
              create: [
                {
                  list: List.NextAction,
                  name: Seed.Trash.PA_A,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
              ],
            },
          },
          {
            list: List.Project,
            name: Seed.Archive.P_A,
            pos: pos(),
            completed: dayjs().subtract(3, 'day').toDate(),
            nextActions: {
              create: [
                {
                  list: List.NextAction,
                  name: Seed.Archive.P_A_A,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
              ],
            },
          },
          {
            list: List.Project,
            name: Seed.Project.A,
            pos: pos(),
            nextActions: {
              create: [
                {
                  list: List.NextAction,
                  name: Seed.Archive.PA_A,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                  completed: new Date(),
                },
                {
                  list: List.NextAction,
                  name: Seed.NextAction.PA.A,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
                {
                  list: List.NextAction,
                  name: Seed.NextAction.PA.B,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
                {
                  list: List.NextAction,
                  name: Seed.NextAction.PA.C,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
              ],
            },
          },
          {
            list: List.Project,
            name: Seed.Project.B,
            pos: pos(),
            nextActions: {
              create: [
                {
                  list: List.NextAction,
                  name: Seed.NextAction.PB.A,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
                {
                  list: List.NextAction,
                  name: Seed.NextAction.PB.B,
                  user: { connect: { id: user.id } },
                  pos: pos(),
                },
              ],
            },
          },
          {
            list: List.NextAction,
            name: Seed.NextAction.A,
            pos: pos(),
          },
          {
            list: List.NextAction,
            name: Seed.NextAction.TA,
            pos: pos(),
            tags: {
              create: [{
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.Context.A },
                  },
                },
              }],
            },
          },
          {
            list: List.NextAction,
            name: Seed.NextAction.TB,
            pos: pos(),
            tags: {
              create: [{
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.Context.B },
                  },
                },
              }],
            },
          },
          {
            list: List.NextAction,
            name: Seed.NextAction.TC,
            pos: pos(),
            tags: {
              create: [{
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.Context.C },
                  },
                },
              }, {
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.WaitingFor.A },
                  },
                },
              }],
            },
          },
          {
            list: List.NextAction,
            name: Seed.NextAction.TD,
            pos: pos(),
            tags: {
              create: [{
                tag: {
                  connect: {
                    userId_name: { userId: user.id, name: Seed.WaitingFor.B },
                  },
                },
                timestamp: dayjs().subtract(15, 'day').toDate(),
              }],
            },
          },
          ...Object.values(Seed.Calendar).map((name) => ({
            list: List.Calendar,
            pos: pos(),
            name: name,
            ...MetaData.CalendarDate[name],
          })),
          {
            list: List.DoIt,
            name: Seed.Archive.DI,
            pos: pos(),
            completed: dayjs().subtract(2, 'day').toDate(),
          },
          {
            list: List.NextAction,
            name: Seed.Archive.NA,
            pos: pos(),
            completed: dayjs().subtract(1, 'day').toDate(),
          },
        ],
      },
    },
    select: defaultUserSelect,
  })
}

export const seedDemoUser = async () => {
  const demoUser = { id: cuid(), email: `${cuid()}@demo.demo`, name: 'Demo User', temporary: true }
  return await seedUser(demoUser)
}

export const deleteAllTemporaryUsers = async () => {
  return await prisma.user.deleteMany({ where: { temporary: true } })
}

export const reseedTestUser = async () => {
  const testUser = await getTestUserAfterSeed()
  return await seedUser(testUser)
}

export const getTestUserAfterSeed = async () => {
  const temporaryUsers = await prisma.user.findMany({ where: { temporary: true } })
  if (temporaryUsers.length > 1) {
    throw new Error('too many temporary users found during testing')
  }
  const user = temporaryUsers[0]
  if (!user) throw new Error('no test user found')
  return user
}
