/*
  Warnings:

  - The `dueTime` column on the `Calendar` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Calendar" ALTER COLUMN "dueDate" SET DATA TYPE TEXT,
DROP COLUMN "dueTime",
ADD COLUMN     "dueTime" TIMESTAMP(3);
