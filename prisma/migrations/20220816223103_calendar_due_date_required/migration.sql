/*
  Warnings:

  - Made the column `dueDate` on table `Calendar` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Calendar" ALTER COLUMN "dueDate" SET NOT NULL;
