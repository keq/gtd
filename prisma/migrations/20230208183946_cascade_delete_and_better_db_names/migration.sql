/*
  Warnings:

  - You are about to drop the `Tag` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `TagsOnThings` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Thing` table. If the table is not empty, all the data it contains will be lost.

*/
-- CreateEnum
CREATE TYPE "list" AS ENUM ('Inbox', 'SomedayMaybe', 'Reference', 'Project', 'DoIt', 'NextAction', 'Calendar');

-- CreateEnum
CREATE TYPE "tag_type" AS ENUM ('Context', 'WaitingFor');

-- DropForeignKey
ALTER TABLE "Tag" DROP CONSTRAINT "Tag_userId_fkey";

-- DropForeignKey
ALTER TABLE "TagsOnThings" DROP CONSTRAINT "TagsOnThings_tagId_fkey";

-- DropForeignKey
ALTER TABLE "TagsOnThings" DROP CONSTRAINT "TagsOnThings_thingId_fkey";

-- DropForeignKey
ALTER TABLE "Thing" DROP CONSTRAINT "Thing_projectId_fkey";

-- DropForeignKey
ALTER TABLE "Thing" DROP CONSTRAINT "Thing_userId_fkey";

-- DropTable
DROP TABLE "Tag";

-- DropTable
DROP TABLE "TagsOnThings";

-- DropTable
DROP TABLE "Thing";

-- DropEnum
DROP TYPE "List";

-- DropEnum
DROP TYPE "TagType";

-- CreateTable
CREATE TABLE "tags" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "type" "tag_type" NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "tags_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "tags_on_things" (
    "thingId" TEXT NOT NULL,
    "tagId" TEXT NOT NULL,
    "timestamp" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "tags_on_things_pkey" PRIMARY KEY ("thingId","tagId")
);

-- CreateTable
CREATE TABLE "things" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "userId" TEXT NOT NULL,
    "list" "list" NOT NULL DEFAULT 'Inbox',
    "pos" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "note" TEXT,
    "completed" TIMESTAMP(3),
    "trashed" TIMESTAMP(3),
    "collapsed" BOOLEAN NOT NULL DEFAULT false,
    "outcome" TEXT,
    "projectId" TEXT,
    "dueDate" TEXT,
    "dueTime" TIMESTAMP(3),

    CONSTRAINT "things_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "tags_userId_name_key" ON "tags"("userId", "name");

-- CreateIndex
CREATE UNIQUE INDEX "things_userId_pos_key" ON "things"("userId", "pos");

-- AddForeignKey
ALTER TABLE "tags" ADD CONSTRAINT "tags_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "tags_on_things" ADD CONSTRAINT "tags_on_things_thingId_fkey" FOREIGN KEY ("thingId") REFERENCES "things"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "tags_on_things" ADD CONSTRAINT "tags_on_things_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "tags"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "things" ADD CONSTRAINT "things_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "things" ADD CONSTRAINT "things_projectId_fkey" FOREIGN KEY ("projectId") REFERENCES "things"("id") ON DELETE SET NULL ON UPDATE CASCADE;
