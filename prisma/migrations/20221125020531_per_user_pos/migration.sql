/*
  Warnings:

  - A unique constraint covering the columns `[userId,position]` on the table `Thing` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "Thing_position_key";

-- CreateIndex
CREATE UNIQUE INDEX "Thing_userId_position_key" ON "Thing"("userId", "position");
