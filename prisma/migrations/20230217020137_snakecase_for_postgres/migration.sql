/*
  Warnings:

  - You are about to drop the column `userId` on the `tags` table. All the data in the column will be lost.
  - The primary key for the `tags_on_things` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `tagId` on the `tags_on_things` table. All the data in the column will be lost.
  - You are about to drop the column `thingId` on the `tags_on_things` table. All the data in the column will be lost.
  - You are about to drop the column `createdAt` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `dueDate` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `dueTime` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `projectId` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `things` table. All the data in the column will be lost.
  - You are about to drop the column `darkTheme` on the `users` table. All the data in the column will be lost.
  - You are about to drop the column `webcalId` on the `users` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[user_id,name]` on the table `tags` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[user_id,pos]` on the table `things` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[webcal_id]` on the table `users` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `user_id` to the `tags` table without a default value. This is not possible if the table is not empty.
  - Added the required column `tag_id` to the `tags_on_things` table without a default value. This is not possible if the table is not empty.
  - Added the required column `thing_id` to the `tags_on_things` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_at` to the `things` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_id` to the `things` table without a default value. This is not possible if the table is not empty.
  - The required column `webcal_id` was added to the `users` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.

*/
-- DropForeignKey
ALTER TABLE "tags" DROP CONSTRAINT "tags_userId_fkey";

-- DropForeignKey
ALTER TABLE "tags_on_things" DROP CONSTRAINT "tags_on_things_tagId_fkey";

-- DropForeignKey
ALTER TABLE "tags_on_things" DROP CONSTRAINT "tags_on_things_thingId_fkey";

-- DropForeignKey
ALTER TABLE "things" DROP CONSTRAINT "things_projectId_fkey";

-- DropForeignKey
ALTER TABLE "things" DROP CONSTRAINT "things_userId_fkey";

-- DropIndex
DROP INDEX "tags_userId_name_key";

-- DropIndex
DROP INDEX "things_userId_pos_key";

-- DropIndex
DROP INDEX "users_webcalId_key";

-- AlterTable
ALTER TABLE "tags" DROP COLUMN "userId",
ADD COLUMN     "user_id" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "tags_on_things" DROP CONSTRAINT "tags_on_things_pkey",
DROP COLUMN "tagId",
DROP COLUMN "thingId",
ADD COLUMN     "tag_id" TEXT NOT NULL,
ADD COLUMN     "thing_id" TEXT NOT NULL,
ADD CONSTRAINT "tags_on_things_pkey" PRIMARY KEY ("thing_id", "tag_id");

-- AlterTable
ALTER TABLE "things" DROP COLUMN "createdAt",
DROP COLUMN "dueDate",
DROP COLUMN "dueTime",
DROP COLUMN "projectId",
DROP COLUMN "updatedAt",
DROP COLUMN "userId",
ADD COLUMN     "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "due_date" TEXT,
ADD COLUMN     "due_time" TIMESTAMP(3),
ADD COLUMN     "project_id" TEXT,
ADD COLUMN     "updated_at" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "user_id" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "users" DROP COLUMN "darkTheme",
DROP COLUMN "webcalId",
ADD COLUMN     "dark_theme" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "webcal_id" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "tags_user_id_name_key" ON "tags"("user_id", "name");

-- CreateIndex
CREATE UNIQUE INDEX "things_user_id_pos_key" ON "things"("user_id", "pos");

-- CreateIndex
CREATE UNIQUE INDEX "users_webcal_id_key" ON "users"("webcal_id");

-- AddForeignKey
ALTER TABLE "tags" ADD CONSTRAINT "tags_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "tags_on_things" ADD CONSTRAINT "tags_on_things_thing_id_fkey" FOREIGN KEY ("thing_id") REFERENCES "things"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "tags_on_things" ADD CONSTRAINT "tags_on_things_tag_id_fkey" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "things" ADD CONSTRAINT "things_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "things" ADD CONSTRAINT "things_project_id_fkey" FOREIGN KEY ("project_id") REFERENCES "things"("id") ON DELETE SET NULL ON UPDATE CASCADE;
