/*
  Warnings:

  - The primary key for the `Context` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- DropForeignKey
ALTER TABLE "NextAction" DROP CONSTRAINT "NextAction_contextId_fkey";

-- AlterTable
ALTER TABLE "Context" DROP CONSTRAINT "Context_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Context_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Context_id_seq";

-- AlterTable
ALTER TABLE "NextAction" ALTER COLUMN "contextId" SET DATA TYPE TEXT;

-- AddForeignKey
ALTER TABLE "NextAction" ADD CONSTRAINT "NextAction_contextId_fkey" FOREIGN KEY ("contextId") REFERENCES "Context"("id") ON DELETE SET NULL ON UPDATE CASCADE;
