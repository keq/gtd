/*
  Warnings:

  - You are about to drop the column `notes` on the `Thing` table. All the data in the column will be lost.
  - You are about to drop the column `position` on the `Thing` table. All the data in the column will be lost.
  - You are about to drop the `Calendar` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `DoIt` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Inbox` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `NextAction` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Project` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Reference` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `SomedayMaybe` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Trash` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[userId,pos]` on the table `Thing` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `pos` to the `Thing` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "List" AS ENUM ('Inbox', 'SomedayMaybe', 'Reference', 'Project', 'DoIt', 'NextAction', 'Calendar');

-- DropForeignKey
ALTER TABLE "Calendar" DROP CONSTRAINT "Calendar_thingId_fkey";

-- DropForeignKey
ALTER TABLE "DoIt" DROP CONSTRAINT "DoIt_thingId_fkey";

-- DropForeignKey
ALTER TABLE "Inbox" DROP CONSTRAINT "Inbox_thingId_fkey";

-- DropForeignKey
ALTER TABLE "NextAction" DROP CONSTRAINT "NextAction_contextId_fkey";

-- DropForeignKey
ALTER TABLE "NextAction" DROP CONSTRAINT "NextAction_projectId_fkey";

-- DropForeignKey
ALTER TABLE "NextAction" DROP CONSTRAINT "NextAction_thingId_fkey";

-- DropForeignKey
ALTER TABLE "Project" DROP CONSTRAINT "Project_thingId_fkey";

-- DropForeignKey
ALTER TABLE "Reference" DROP CONSTRAINT "Reference_thingId_fkey";

-- DropForeignKey
ALTER TABLE "SomedayMaybe" DROP CONSTRAINT "SomedayMaybe_thingId_fkey";

-- DropForeignKey
ALTER TABLE "Trash" DROP CONSTRAINT "Trash_thingId_fkey";

-- DropIndex
DROP INDEX "Thing_userId_position_key";

-- AlterTable
ALTER TABLE "Thing" DROP COLUMN "notes",
DROP COLUMN "position",
ADD COLUMN     "collapsed" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "contextId" TEXT,
ADD COLUMN     "dueDate" TEXT,
ADD COLUMN     "dueTime" TIMESTAMP(3),
ADD COLUMN     "list" "List" NOT NULL DEFAULT 'Inbox',
ADD COLUMN     "note" TEXT,
ADD COLUMN     "outcome" TEXT,
ADD COLUMN     "pos" TEXT NOT NULL,
ADD COLUMN     "projectId" TEXT,
ADD COLUMN     "trashed" TIMESTAMP(3),
ADD COLUMN     "waitingFor" TEXT;

-- DropTable
DROP TABLE "Calendar";

-- DropTable
DROP TABLE "DoIt";

-- DropTable
DROP TABLE "Inbox";

-- DropTable
DROP TABLE "NextAction";

-- DropTable
DROP TABLE "Project";

-- DropTable
DROP TABLE "Reference";

-- DropTable
DROP TABLE "SomedayMaybe";

-- DropTable
DROP TABLE "Trash";

-- CreateIndex
CREATE UNIQUE INDEX "Thing_userId_pos_key" ON "Thing"("userId", "pos");

-- AddForeignKey
ALTER TABLE "Thing" ADD CONSTRAINT "Thing_contextId_fkey" FOREIGN KEY ("contextId") REFERENCES "Context"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Thing" ADD CONSTRAINT "Thing_projectId_fkey" FOREIGN KEY ("projectId") REFERENCES "Thing"("id") ON DELETE SET NULL ON UPDATE CASCADE;
