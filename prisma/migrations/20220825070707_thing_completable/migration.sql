/*
  Warnings:

  - You are about to drop the column `completed` on the `DoIt` table. All the data in the column will be lost.
  - You are about to drop the column `completed` on the `NextAction` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "DoIt" DROP COLUMN "completed";

-- AlterTable
ALTER TABLE "NextAction" DROP COLUMN "completed";

-- AlterTable
ALTER TABLE "Thing" ADD COLUMN     "completed" BOOLEAN NOT NULL DEFAULT false;
