## Local Development Setup

1. Install and set up postgres
2. Create database with C collation: `createdb --template=template0 --lc-collate=C nextofthing`
3. `yarn install`
4. `npx prisma db push`
5. `npx prisma db seed`
6. Add a `.env` file at project root and specify values for the following env vars:

```sh
DATABASE_URL="postgresql://user@localhost:5432/nextofthing?schema=public"
GITLAB_ID=""
GITLAB_SECRET=""
GITHUB_ID=""
GITHUB_SECRET=""
NEXTAUTH_SECRET=""
NEXTAUTH_URL="http://localhost:3000"
```

6. `yarn dev`

## Tests

- `yarn test`
- `yarn test:e2e`
  - `yarn start:e2e` first
  - assumes fresh seeded data
- `npx playwright codegen http://localhost:3000/ --load-storage .storageState.json`
- `yarn test:e2e -g "trash" --debug`

## Tips

- Use `npx prisma studio` to get a nice web UI for the database
- Use `openssl rand -base64 32` to generate `NEXTAUTH_SECRET`
- Build with `ANALYZE=true` to get build size analysis reports
- `docker run --rm -it --entrypoint sh <IMAGE>` to debug build steps

## Testing gitlab-ci.yml

- `sudo gitlab-runner exec docker tests`
- `sudo gitlab-runner exec docker --env SSH_PRIVATE_KEY="$(cat ~/.ssh/id_ed25519)" deploy`

https://stackoverflow.com/a/36358790

## Docker Compose

- `docker compose up`
- `docker compose up -d`
- `docker compose up --force-recreate --build`
- `docker compose exec nextofthing /bin/sh`
